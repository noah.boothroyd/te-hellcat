import './assets/reset.css'
import 'uno.css'
import App from './App.vue'

createApp(App).use(createPinia()).mount('#app')
