import type { EventBusKey } from '@vueuse/core'
import type * as Monaco from 'monaco-editor'

export const errorRangeKey: EventBusKey<Monaco.IRange> = Symbol('symbol-key')
