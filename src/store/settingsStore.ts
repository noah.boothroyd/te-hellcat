// Log results of fetch and push of email settings to datadog
import type { IFolderTag } from 'types/platformData'

export const useSettingsStore = defineStore('settings', {
  state: () => {
    return {
      // General Settings
      email_id: null as number | null,
      email_user_id: null as string | null,
      email_content_id: null as string | null,
      email_org_id: null as number | null,
      email_creator_version: 'te-hellcat',

      email_name: null as string | null,
      email_subject: null as string | null,
      email_description: null as string | null,
      email_preheader: null as string | null,
      email_thumbnail: '',
      email_skip_dedupe: false,
      email_transactional: false,
      email_created_at: new Date(new Date().setDate(new Date().getDate() - 7)),
      email_last_updated_at: new Date(new Date().setDate(new Date().getDate() - 1)),

      email_folders: [] as IFolderTag[],
      email_tags: [] as IFolderTag[],

      // Compliance Settings
      compliance_notification_threshold: 0,
      compliance_notification_recipients: [],
      compliance_disclaimer_override_text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi. Ac tortor dignissim convallis aenean et. Arcu non sodales neque sodales ut etiam sit amet nisl. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus.',
      compliance_override_default: false,

      // Access Settings
      email_access_item_id: null as string | null,
      access_allusers: false,
      access_marketpartners: false,
      access_editing: false,

      // Integration Settings

      // Style Settings

      // Published state
      is_published: false,
    }
  },
  actions: {
    // Get initial settings
    async fetchEmailSettings() {},
  },
})
