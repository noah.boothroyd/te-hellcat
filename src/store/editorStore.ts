import type * as Monaco from 'monaco-editor'

import { createDiscreteApi } from 'naive-ui'
// Log results fetch and push of email content to datadog

const emailHTML = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n\n<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="">\n<head>\n\t\n\t<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n\t<meta name="viewport" content="width=device-width">\n\t<title></title>\n\t<style></style>\n</head>\n<body style="">\n\t\n\t<!-- prevent Gmail on iOS font size manipulation -->\n\t<div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>\n</body>\n</html>'

const { message } = createDiscreteApi(['message'])

export const useEditorStore = defineStore('editor', {
  state: () => {
    return {
      current_model: emailHTML,
      current_line: '',
      current_word: {} as Monaco.editor.IWordAtPosition,
      current_position: {} as Monaco.IPosition,
    }
  },
  getters: {
    getLineNumber: state => state.current_position.lineNumber,
    getColumnNumber: state => state.current_position.column,
  },
  actions: {
    async fetchEmailContent() {
      try {
        // Replace with fetch from database
        this.current_model = emailHTML
      }
      catch (error) {
        if (error instanceof Error)
          message.error(`${error.name}: ${error.message}`)
      }
    },
  },
})
