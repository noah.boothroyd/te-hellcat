import type * as Monaco from 'monaco-editor'
// Log results of fetch and push of errors to datadog

export const useErrorStore = defineStore('errors', {
  state: () => {
    return {
      markers: [] as Partial<Monaco.editor.IMarkerData[]>,
    }
  },
  getters: {
    getErrors: state => state.markers.filter(marker => marker?.severity === 8),
    getWarnings: state => state.markers.filter(marker => marker?.severity === 4),
    getInfo: state => state.markers.filter(marker => marker?.severity === 2),
  },
  actions: {
    // Fetch initial warning/info markers saved in the DB that were ignored
    async fetchWarningMarkers() {},
  },
})
