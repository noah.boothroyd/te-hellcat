export interface INamespace {
  name: string
  label: string
  parent?: string
  hint: string
  subnamespaces?: INamespace[]
  properties: IProperty[]
}
export interface IProperty {
  label: string
  property: string
  type: string
  location: string
  hint?: string
}
