export default [
  // Liquid token colors
  { token: 'tag.liquid', foreground: '335161' },
  { token: 'filter.liquid', foreground: '2b805b' },
  { token: 'variable.liquid', foreground: '26A66F' },
  { token: 'namespace.liquid', foreground: '753BBD' },
  { token: 'property.liquid', foreground: '0072CE' },
  { token: 'string.liquid.html', foreground: '335161' },
  { token: 'number.liquid.html', foreground: '26A66F' },
  { token: 'operator.liquid.html', foreground: 'FF585D' },
  // HTML token colors
  { token: 'metatag.content.html', foreground: '753BBD' },
  { token: 'attribute.value.html', foreground: '0072CE' },
  { token: 'tag.html.html', foreground: '002234' },
  { token: 'tag.html', foreground: '002234' },
  { token: 'attribute.name.html', foreground: '335161' },
  // CSS Token Colors
  { token: 'tag.css', foreground: '002234' },
  { token: 'attribute.name.css', foreground: '335161' },
]
