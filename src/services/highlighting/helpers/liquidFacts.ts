export const builtInLiquidTags = ['if', 'else', 'elseif', 'elsif', 'endif', 'render', 'assign', 'capture', 'endcapture', 'case', 'endcase', 'comment', 'endcomment', 'cycle', 'decrement', 'for', 'endfor', 'include', 'increment', 'layout', 'raw', 'endraw', 'render', 'tablerow', 'endtablerow', 'unless', 'endunless']

export const builtInLiquidFilters = ['abs', 'append', 'at_least', 'at_most', 'capitalize', 'ceil', 'compact', 'date', 'default', 'divided_by', 'downcase', 'escape', 'escape_once', 'first', 'floor', 'join', 'json', 'last', 'lstrip', 'map', 'minus', 'modulo', 'newline_to_br', 'plus', 'prepend', 'remove', 'remove_first', 'replace', 'replace_first', 'reverse', 'round', 'rstrip', 'size', 'slice', 'sort', 'sort_natural', 'split', 'strip', 'strip_html', 'strip_newlines', 'times', 'truncate', 'truncatewords', 'uniq', 'upcase', 'url_decode', 'url_encode', 'where']

export const liquidConstants = ['true', 'false', '??']
export const liquidOperators = ['!==', '==', '!=', '>', '<', '>=', '<=']
