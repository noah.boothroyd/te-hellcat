import type { languages } from 'monaco-editor'
import { TESLDataInstance } from 'api/TESLData'
import { builtInLiquidFilters, builtInLiquidTags, liquidConstants, liquidOperators } from './helpers/liquidFacts'

export const highlighterConfig = <languages.IMonarchLanguage>{
  defaultToken: '',
  tokenPostfix: '.html',
  ignoreCase: true,

  builtinTags: builtInLiquidTags,

  builtinFilters: builtInLiquidFilters,

  builtInNamespaces: TESLDataInstance.getAllNamespaceNames(),

  builtInProperties: TESLDataInstance.getAllPropertyNames(true),

  constants: liquidConstants,
  operators: liquidOperators,

  symbol: /[=><!]+/,
  identifier: /[a-zA-Z_][\w]*/,

  tokenizer: {
    root: [
      [/<!DOCTYPE/, 'metatag', '@doctype'],
      [/<!--/, 'comment', '@comment'],
      [/\{\%\s*comment\s*\%\}/, 'comment.start.liquid', '@comment'],
      [/\{\{/, { token: '@rematch', switchTo: '@liquidState.root' }],
      [/\{\%/, { token: '@rematch', switchTo: '@liquidState.root' }],
      [/(<)([\w\-]+)(\/>)/, ['delimiter.html', 'tag.html', 'delimiter.html']],
      [/(<)(style)/, ['delimiter', { token: 'tag', next: '@style' }]],
      [/(<)([:\w]+)/, ['delimiter.html', { token: 'tag.html', next: '@otherTag' }]],
      [/(<\/)([\w\-]+)/, ['delimiter.html', { token: 'tag.html', next: '@otherTag' }]],
      [/</, 'delimiter.html'],
      [/\{/, 'delimiter.html'],
      [/[^<{]+/], // text
    ],

    doctype: [
      [/[^>]+/, 'metatag.content'],
      [/>/, 'metatag', '@pop'],
    ],
    comment: [
      [/\{\%\s*endcomment\s*\%\}/, 'comment.end.liquid', '@pop'],
      [/-->/, 'comment', '@pop'],
      [/./, 'comment.content.liquid'],
      [/[^-]+/, 'comment.content'],
      [/./, 'comment.content'],
    ],

    otherTag: [
      [/\/?>/, 'delimiter.html', '@pop'],
      [
        /\{\{/,
        {
          token: '@rematch',
          switchTo: '@liquidState.otherTag',
        },
      ],
      [
        /\{\%/,
        {
          token: '@rematch',
          switchTo: '@liquidState.otherTag',
        },
      ],
      [/"([^"]*)"/, 'attribute.value'],
      [/'([^']*)'/, 'attribute.value'],
      [/[\w\-]+/, 'attribute.name'],
      [/=/, 'delimiter'],
      [/[ \t\r\n]+/], // whitespace
    ],

    // -- BEGIN <style> tags handling

    // After <style
    style: [
      [/type/, 'attribute.name', '@styleAfterType'],
      [/"([^"]*)"/, 'attribute.value'],
      [/'([^']*)'/, 'attribute.value'],
      [/[\w\-]+/, 'attribute.name'],
      [/=/, 'delimiter'],
      [
        />/,
        {
          token: 'delimiter',
          next: '@styleEmbedded',
          nextEmbedded: 'text/css',
        },
      ],
      [/[ \t\r\n]+/], // whitespace
      [/(<\/)(style\s*)(>)/, ['delimiter', 'tag', { token: 'delimiter', next: '@pop' }]],
    ],

    // After <style ... type
    styleAfterType: [
      [/=/, 'delimiter', '@styleAfterTypeEquals'],
      [
        />/,
        {
          token: 'delimiter',
          next: '@styleEmbedded',
          nextEmbedded: 'text/css',
        },
      ], // cover invalid e.g. <style type>
      [/[ \t\r\n]+/], // whitespace
      [/<\/style\s*>/, { token: '@rematch', next: '@pop' }],
    ],

    // After <style ... type =
    styleAfterTypeEquals: [
      [
        /"([^"]*)"/,
        {
          token: 'attribute.value',
          switchTo: '@styleWithCustomType.$1',
        },
      ],
      [
        /'([^']*)'/,
        {
          token: 'attribute.value',
          switchTo: '@styleWithCustomType.$1',
        },
      ],
      [
        />/,
        {
          token: 'delimiter',
          next: '@styleEmbedded',
          nextEmbedded: 'text/css',
        },
      ], // cover invalid e.g. <style type=>
      [/[ \t\r\n]+/], // whitespace
      [/<\/style\s*>/, { token: '@rematch', next: '@pop' }],
    ],

    // After <style ... type = $S2
    styleWithCustomType: [
      [
        />/,
        {
          token: 'delimiter',
          next: '@styleEmbedded.$S2',
          nextEmbedded: '$S2',
        },
      ],
      [/"([^"]*)"/, 'attribute.value'],
      [/'([^']*)'/, 'attribute.value'],
      [/[\w\-]+/, 'attribute.name'],
      [/=/, 'delimiter'],
      [/[ \t\r\n]+/], // whitespace
      [/<\/style\s*>/, { token: '@rematch', next: '@pop' }],
    ],

    styleEmbedded: [
      [/<\/style/, { token: '@rematch', next: '@pop', nextEmbedded: '@pop' }],
      [/[^<]+/, ''],
    ],

    // -- END <style> tags handling

    liquidState: [
      [/\{\{/, 'delimiter.output.opening.liquid'],
      [/\}\}/, { token: 'delimiter.output.closing.liquid', switchTo: '@$S2.$S3' }],
      [/\{\%/, 'delimiter.tag.opening.liquid'],
      [/raw\s*\%\}/, 'delimiter.tag.liquid', '@liquidRaw'],
      [/\%\}/, { token: 'delimiter.tag.closing.liquid', switchTo: '@$S2.$S3' }],
      { include: 'liquidRoot' },
    ],

    liquidRaw: [
      [/^(?!\{\%\s*endraw\s*\%\}).+/],
      [/\{\%/, 'delimiter.tag.liquid'],
      [/@identifier/],
      [/\%\}/, { token: 'delimiter.tag.liquid', next: '@root' }],
    ],

    liquidRoot: [
      [/\d+(\.\d+)?/, 'number.liquid'],
      [/"[^"]*"/, 'string.liquid'],
      [/'[^']*'/, 'string.liquid'],
      [/\s+/],
      [
        /@symbol/,
        {
          cases: {
            '@operators': 'operator.liquid',
            '@default': '',
          },
        },
      ],
      [/\./],
      [
        /@identifier/,
        {
          cases: {
            '@constants': 'keyword.liquid',
            '@builtinFilters': 'filter.liquid',
            '@builtinTags': 'tag.liquid',
            '@builtInNamespaces': 'namespace.liquid',
            '@builtInProperties': 'property.liquid',
            '@default': 'variable.liquid',
          },
        },
      ],
      [/[^}|%]/, 'variable.liquid'],
    ],

  },
}
