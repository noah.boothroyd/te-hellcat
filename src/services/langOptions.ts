import type { languages } from 'monaco-editor'
import { emailHtmlData } from './emailHTMLData'

type HTMLFormatConfiguration = languages.html.HTMLFormatConfiguration
type HTMLDataConfiguration = languages.html.HTMLDataConfiguration

const formatOptions: HTMLFormatConfiguration = {
  tabSize: 4,
  insertSpaces: false,
  wrapLineLength: 130,
  unformatted: 'default": "a, abbr, acronym, b, bdo, big, br, button, cite, code, dfn, div, em, i, img, input, kbd, label, map, object, q, samp, select, small, span, strong, sub, sup, textarea, tt, var',
  contentUnformatted: 'pre, div',
  indentInnerHtml: false,
  preserveNewLines: true,
  maxPreserveNewLines: undefined,
  indentHandlebars: true,
  endWithNewline: false,
  extraLiners: 'head, body, /html',
  wrapAttributes: 'auto',
}

const dataOptions: HTMLDataConfiguration = {
  useDefaultDataProvider: false,
  dataProviders: {
    emailHTML: emailHtmlData,
  },
}

export default {
  format: formatOptions,
  suggest: {},
  data: dataOptions,
}
