import type * as Monaco from 'monaco-editor'
import { languages } from 'monaco-editor'

const EMPTY_ELEMENTS: string[] = [
  'area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'keygen',
  'link',
  'menuitem',
  'meta',
  'param',
  'source',
  'track',
  'wbr',
]

export default {
  wordPattern: /(-?\d*\.\d\w*)|([^\`\~\!\@\$\^\&\*\(\)\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\s]+)/g,

  comments: {
    blockComment: ['<!--', '-->'],
  },

  brackets: [
    ['<!--', '-->'],
    ['<', '>'],
    ['{{', '}}'],
    ['{%', '%}'],
    ['{', '}'],
    ['(', ')'],
  ],

  autoClosingPairs: [
    { open: '{', close: '}' },
    { open: '%', close: '%' },
    { open: '[', close: ']' },
    { open: '(', close: ')' },
    { open: '"', close: '"' },
    { open: '\'', close: '\'' },
  ],

  surroundingPairs: [
    { open: '<', close: '>' },
    { open: '"', close: '"' },
    { open: '\'', close: '\'' },
  ],

  onEnterRules: [
    {
      beforeText: new RegExp(
        '<(?!(?:' + EMPTY_ELEMENTS.join('|') + '))(\\w[\\w\\d]*)([^/>]*(?!/)>)[^<]*$',
        'i',
      ),
      afterText: /^<\/(\w[\w\d]*)\s*>$/i,
      action: {
        indentAction: languages.IndentAction.IndentOutdent,
      },
    },
    {
      beforeText: new RegExp(
        '<(?!(?:' + EMPTY_ELEMENTS.join('|') + '))(\\w[\\w\\d]*)([^/>]*(?!/)>)[^<]*$',
        'i',
      ),
      action: { indentAction: languages.IndentAction.Indent },
    },
  ],
  folding: {
    markers: {
      start: /^\\s*<!--\\s*#region\\b.*-->/,
      end: /^\\s*<!--\\s*#endregion\\b.*-->/,
    },
  },
} as Monaco.languages.LanguageConfiguration
