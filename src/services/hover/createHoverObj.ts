import type * as Monaco from 'monaco-editor'
import { Range } from 'monaco-editor'
import { TESLDataInstance } from 'api/TESLData'
import filterList from 'data/filterList'
import Turndown from 'turndown'

/**
 * Creates the hover objects for the provider from API data
 * @param word The current word being hovered over
 * @param position Position in the document at which completions are requested
 */

export async function createHoverObj(
  currentword: Monaco.editor.IWordAtPosition,
  position: Monaco.IPosition,
): Promise<Monaco.languages.Hover | undefined> {
  const hoverObject: Monaco.languages.Hover = {
    range: new Range(position.lineNumber, currentword.startColumn, position.lineNumber, currentword.endColumn),
    contents: [],
  }
  const isNamespace = TESLDataInstance.getAllNamespaceNames().includes(currentword.word)
  const isProperty = TESLDataInstance.getAllPropertyNames().includes(currentword.word)
  const isFilter = filterList.includes(currentword.word)

  if (isNamespace)
    hoverObject.contents = await createNamespaceHoverObj(currentword.word) || []
  if (isProperty)
    hoverObject.contents = await createPropertyHoverObj(currentword.word) || []
  if (isFilter)
    hoverObject.contents = await createFilterHoverObj(currentword.word) || []

  return hoverObject
}

/**
 * Find the namespace object that matches the hover and format it for Monaco
 * @param namespace The current namespace being hovered over
 * @return Promise<Monaco.IMarkdownString[] | undefined>
 */

export async function createNamespaceHoverObj(namespace: string): Promise<Monaco.IMarkdownString[] | undefined> {
  const namespaceObj = TESLDataInstance.getNamespaceObjByName(namespace)
  if (!namespaceObj)
    return undefined
  return [{
    value: `**Namespace**: *${namespaceObj.label}*`,
  },
  {
    value: namespaceObj.hint,
  }]
}

/**
 * Find the property object that matches the hover and format it for Monaco
 * @param property The current property being hovered over
 * @return Promise<Monaco.IMarkdownString[] | undefined>
 */

export async function createPropertyHoverObj(property: string): Promise<Monaco.IMarkdownString[] | undefined> {
  const propertyObj = TESLDataInstance.getPropertyObjByProperty(property)
  if (!propertyObj)
    return undefined
  return [{
    value: `**Property**: ${propertyObj.label} | *${propertyObj.type}*`,
  },
  {
    value: `${propertyObj.hint}\n\n**Platform Location**: *${propertyObj.location}*`,
  }]
}

/**
 * Convert filter info into markdown and format it for Monaco
 * @param filter The current filter being hovered over
 * @return Promise<Monaco.IMarkdownString[] | undefined>
 */
export async function createFilterHoverObj(filter: string): Promise<Monaco.IMarkdownString[] | undefined> {
  const link = `https://shopify.github.io/liquid/filters/${filter}/`
  const page = await fetchFilterData(filter, link)
  if (!page)
    return undefined

  const parser = new DOMParser()
  const filterDocument = parser.parseFromString(page, 'text/html')

  // Clean up the doc - we only need what's in the content div.
  const contentDiv = filterDocument.getElementsByClassName('content')
  const menuButton = filterDocument.getElementsByClassName('menu-button')
  const filterh1 = filterDocument.getElementsByTagName('h1')
  contentDiv[0].removeChild(menuButton[0])
  contentDiv[0].removeChild(filterh1[0])
  const hoverString = contentDiv[0].innerHTML.trim()

  // Markdown conversion
  const turndownService = new Turndown({ codeBlockStyle: 'fenced' })
  const markdownString = turndownService.turndown(hoverString)

  return [{
    value: `**Filter**: ${filter}`,
  },
  {
    value: markdownString,
  },
  {
    value: `[Liquid Reference](${link})`,
  }]
}

/**
 * Fetch filter info from the Liquid Shopify Docs
 * @param filter The current filter being hovered over
 * @param endpoint The link to the Liquid docs being fetched
 */

export async function fetchFilterData(filter: string, endpoint: string) {
  if (!filter || !filterList.includes(filter))
    return undefined
  const response = await fetch(endpoint)
  return response.text()
}
