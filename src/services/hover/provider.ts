import type * as Monaco from 'monaco-editor'
import TESLTokenizer from '../autocomplete/helpers/TESLtokenizer'
import { createHoverObj } from './createHoverObj'

/**
 * Register the tag hover completion provider
 * @param monaco monaco self
 */

export default function registerHoverProvider(monaco: typeof Monaco | undefined) {
  if (!monaco) {
    console.error('\'monaco\' should be either declared on window or passed as first parameter')
    return
  }
  const hoverProvider: Monaco.IDisposable = monaco.languages.registerHoverProvider('html', {
    provideHover(model, position) {
      const currentWord: Monaco.editor.IWordAtPosition | null = model.getWordAtPosition(position)
      if (!currentWord)
        return undefined

      try {
        return createHoverObj(currentWord, position).then((response) => {
          return response && TESLTokenizer.isValidTESLLocation(model, position) ? response : undefined
        })
      }
      catch (error) {
        console.log(error)
        return undefined
      }
    },
  })
  return hoverProvider
}
