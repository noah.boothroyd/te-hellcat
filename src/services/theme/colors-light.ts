export default {
  // Scrollbar
  'scrollbar.shadow': '#00263A', // Scrollbar shadow to indicate that the view is scrolled.
  'scrollbarSlider.background': '#00263A', // Slider background color.
  'scrollbarSlider.hoverBackground': '#667d89', // Slider background color when hovering.
  'scrollbarSlider.activeBackground': '#335161', // Slider background color when active.
  // Editor
  'foreground': '#00263A', // Overall foreground color. This color is only used if not overridden by a component.
  'editor.background': '#fdfdfd', // Editor background color.
  'editor.foreground': '#00263A', // Editor default foreground color.
  'editorHoverWidget.background': '#EDF3F7', // Background color of the editor hover.
  'editorHoverWidget.border': '#00263A', // Border color of the editor hover.
  'editorCursor.foreground': '#00263A', // Color of the editor cursor.
  // Gutter
  'editorLineNumber.foreground': '#005ba5', // Color of editor line numbers.
  'editorLineNumber.activeForeground': '#66aae2', // Color of editor active line number.
  'editorGutter.background': '#EDF3F7', // Background color of the editor gutter. The gutter contains the glyph margins and the line numbers.
  // Error and Warning Squigglies
  'editorError.foreground': '#FF585D', // Foreground color of error squigglies in the editor.
  'editorError.border': '#FF585D', // Border color of error squigglies in the editor.
  'editorWarning.foreground': '#FFA400', // Foreground color of warning squigglies in the editor.
  'editorWarning.border': '#FFA400', // Border color of warning squigglies in the editor.
}
