import { TESLAValidator } from 'teslahtml-validator'

self.addEventListener('message', (event) => {
  const { html, version } = event.data
  const ruleset = { ...TESLAValidator.emailHTMLRuleset, ...TESLAValidator.DataRuleset }
  try {
    const markers = TESLAValidator.verify(html, ruleset)
    self.postMessage({ markers, version })
  }
  catch (error) {
    // Log to datadog
    console.log('error:')
    console.log(error)
  }
})
