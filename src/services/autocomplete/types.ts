export interface Token {
  readonly offset: number
  readonly type: string
}
