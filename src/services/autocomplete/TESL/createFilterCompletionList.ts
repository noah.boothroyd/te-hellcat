// Pull filter list from the eventual TESL/Liquid Service here
import type * as Monaco from 'monaco-editor'
import filters from 'data/filterList'
import TESLTokenizer from '../helpers/TESLtokenizer'

/**
 * Returns all applicable liquid tags for abbreviation within valid TESL brackets
 * @param model TextModel in which completions are requested
 * @param position Position in the document at which completions are requested
 */
export default function createFilterCompletionList(
  monaco: typeof Monaco,
  model: Monaco.editor.ITextModel,
  position: Monaco.IPosition,
): Monaco.languages.CompletionList | undefined {
  const completionItems: Monaco.languages.CompletionItem[] = []
  const currentWord = model.getWordUntilPosition(position)
  // Check if we are after a pipe:
  const pipeRegex = /([\w\s]+)(?=[|])/g
  const colonRegex = /([\w\s]+)(?=[:])/g
  const currentBracketsOffset = TESLTokenizer.latestBrackets(model, position) || 0
  const currentBrackets = model.getValueInRange({
    startColumn: 0 + currentBracketsOffset,
    startLineNumber: position.lineNumber,
    endColumn: position.column,
    endLineNumber: position.lineNumber,
  })
  const pipeMatch = currentBrackets.match(pipeRegex)
  const colonMatch = currentBrackets.match(colonRegex)

  if (pipeMatch && !colonMatch) {
    const range: Monaco.IRange = {
      startLineNumber: position.lineNumber,
      endLineNumber: position.lineNumber,
      startColumn: currentWord.startColumn,
      endColumn: currentWord.endColumn,
    }
    filters.forEach((filter) => {
      const item: Monaco.languages.CompletionItem = {
        kind: monaco.languages.CompletionItemKind.Function,
        label: filter,
        detail: 'filter',
        range,
        insertText: ` ${filter}`,
      }

      completionItems.push(item)
    })
  }
  return completionItems.length ? { suggestions: completionItems, incomplete: true } : undefined
}
