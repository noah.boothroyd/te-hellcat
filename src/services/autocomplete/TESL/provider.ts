import type * as Monaco from 'monaco-editor'
import TESLTokenizer from '../helpers/TESLtokenizer'
import createFilterCompletionList from './createFilterCompletionList'

/**
 * Register the TESL filter provider.
 * @param monaco monaco self
 */
export default function registerFilterProvider(monaco: typeof Monaco | undefined) {
  if (!monaco) {
    console.error('\'monaco\' should be either declared on window or passed as first parameter')
    return
  }
  const filterProvider: Monaco.IDisposable = monaco.languages.registerCompletionItemProvider('html', {
    triggerCharacters: ['|'],
    provideCompletionItems(model, position) {
      return TESLTokenizer.isValidTESLLocation(model, position) ? createFilterCompletionList(monaco!, model, position) : undefined
    },
  })
  return filterProvider
}
