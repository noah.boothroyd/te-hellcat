import type * as Monaco from 'monaco-editor'
import { TESLDataInstance } from 'api/TESLData'

import TESLTokenizer from '../helpers/TESLtokenizer'
import makePlaceholderSuggestions from './makePlaceholderSuggestions'

/**
 * Returns all applicable liquid placeholders for abbreviation within valid TESL brackets
 * @param model TextModel in which completions are requested
 * @param position Position in the document at which completions are requested
 */
export default function createPlaceholderCompletionList(
  monaco: typeof Monaco,
  model: Monaco.editor.ITextModel,
  position: Monaco.IPosition,
): Monaco.languages.CompletionList | undefined {
  let completionItems: Monaco.languages.CompletionItem[] = []

  const currentWord = model.getWordUntilPosition(position)
  const dotRegex = /(\w+)(?=[.])/g
  const pipeRegex = /([\w\s]+)(?=[|])/g
  const currentBracketsOffset = TESLTokenizer.latestBrackets(model, position) || 0
  const currentBrackets = model.getValueInRange({
    startColumn: 0 + currentBracketsOffset,
    startLineNumber: position.lineNumber,
    endColumn: position.column,
    endLineNumber: position.lineNumber,
  })
  const match = currentBrackets.match(dotRegex)
  const pipeMatch = currentBrackets.match(pipeRegex)
  const currentTag = match ? match.reverse()[0] : undefined

  if (!pipeMatch) {
    // Get our top level namespaces
    const topLevelNamespaces = TESLDataInstance.flattenedNamespaceData.filter(item => !item.parent)
    const namespaceSuggestions = makePlaceholderSuggestions(
      monaco,
      'namespace',
      position.lineNumber,
      currentWord,
      topLevelNamespaces,
    )
    completionItems = namespaceSuggestions
  }

  if (currentTag && !pipeMatch) {
    completionItems = []
    const subNamespaces = TESLDataInstance.flattenedNamespaceData.filter(item => item.parent === currentTag)
    const subNamespaceSuggestions = makePlaceholderSuggestions(
      monaco,
      'namespace',
      position.lineNumber,
      currentWord,
      subNamespaces,
      true,
    )
    completionItems = subNamespaceSuggestions
    const propertySuggestions = makePlaceholderSuggestions(
      monaco,
      'property',
      position.lineNumber,
      currentWord,
      [],
      false,
      currentTag,
    )
    completionItems = completionItems.concat(propertySuggestions)
  }

  return completionItems.length ? { suggestions: completionItems, incomplete: true } : undefined
}

