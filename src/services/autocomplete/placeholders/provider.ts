import type * as Monaco from 'monaco-editor'
import TESLTokenizer from '../helpers/TESLtokenizer'
import createPlaceholderCompletionList from './createPlaceholderCompletionList'

/**
 * Register the dynamic data completion provider
 * @param monaco monaco self
 */
export default function registerPlaceHolderProvider(monaco: typeof Monaco | undefined) {
  if (!monaco) {
    console.error('\'monaco\' should be either declared on window or passed as first parameter')
    return
  }
  const placeholderProvider: Monaco.IDisposable = monaco.languages.registerCompletionItemProvider('html', {
    triggerCharacters: ['.'],
    provideCompletionItems(model, position) {
      return TESLTokenizer.isValidTESLLocation(model, position) ? createPlaceholderCompletionList(monaco!, model, position) : undefined
    },
  })
  return placeholderProvider
}
