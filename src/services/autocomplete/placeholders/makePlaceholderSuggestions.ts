import type * as Monaco from 'monaco-editor'
import type { INamespace, IProperty } from 'types/tags'
import { TESLDataInstance } from 'api/TESLData'
/**
 * Create & return property or namespace list based on the current word.
 * @param monaco Monaco
 * @param type 'namespace' | 'property'
 * @param lineNumber number
 * @param word Monaco.editor.IWordAtPosition
 * @param snippetNamespaceKeys INamespace[]
 * @param sort boolean
 * @param namespace string | undefined
 */

export default function makePlaceholderSuggestions(
  monaco: typeof Monaco,
  type: 'namespace' | 'property',
  lineNumber: number,
  word: Monaco.editor.IWordAtPosition,
  snippetNamespaceKeys: INamespace[], // namespace
  sort = false,
  namespace?: string | undefined, // property
) {
  const snippetCompletions: Monaco.languages.CompletionItem[] = []

  const range: Monaco.IRange = {
    startLineNumber: lineNumber,
    endLineNumber: lineNumber,
    startColumn: word.startColumn,
    endColumn: word.endColumn,
  }

  if (type === 'namespace') {
    if (!snippetNamespaceKeys)
      return []
    snippetNamespaceKeys.forEach((snippetNamespaceKey: INamespace) => {
      const item: Monaco.languages.CompletionItem = {
        kind: monaco.languages.CompletionItemKind.Property,
        label: snippetNamespaceKey.label,
        detail: 'Namespace',
        range,
        insertText: snippetNamespaceKey.name,
        sortText: sort ? 'a' : undefined,
      }

      snippetCompletions.push(item)
    })
  }

  if (type === 'property') {
    if (namespace === undefined)
      return []
    const snippetPropertyKeys: IProperty[] = TESLDataInstance.getPropertiesByNamespace(namespace) || []
    if (snippetPropertyKeys.length === 0)
      return []
    snippetPropertyKeys.forEach((snippetPropertyKey: IProperty) => {
      const item: Monaco.languages.CompletionItem = {
        kind: monaco.languages.CompletionItemKind.Property,
        label: snippetPropertyKey.label,
        detail: 'property',
        range,
        insertText: `${snippetPropertyKey.property}`,
      }

      snippetCompletions.push(item)
    })
  }
  return snippetCompletions
}
