import type * as Monaco from 'monaco-editor'
import type { Token } from '../types'

const TESLtokenizer = {
  tokenizer(model: Monaco.editor.ITextModel, position: Monaco.IPosition): Token[] {
    const { lineNumber } = position

    const _tokenization = (model as any).tokenization._tokenization
    const _tokenizationStateStore = _tokenization._tokenizationStateStore
    const _tokenizationSupport = _tokenizationStateStore.tokenizationSupport

    const state = _tokenizationStateStore.getBeginState(lineNumber - 1).clone()
    const tokenizationResult = _tokenizationSupport.tokenize(model.getLineContent(lineNumber), true, state, 0)
    return tokenizationResult.tokens
  },
  latestBrackets(model: Monaco.editor.ITextModel, position: Monaco.IPosition) {
    const bracketTokens = this.tokenizer(model, position)
    const { column } = position

    // Find token at the cursor:
    for (let i = bracketTokens.length - 1; i >= 0; i--) {
      if (column - 1 > bracketTokens[i].offset) {
        // function
        const beforeTokens = bracketTokens.slice(0, i)
        const currentOpeningTags = beforeTokens.reverse().find(token => token.type === 'delimiter.output.opening.liquid.html' || token.type === 'delimiter.tag.opening.liquid.html')
        return currentOpeningTags?.offset
      }
    }
  },
  checkDelimiter(tokens: Token[], index: number, type: 'before' | 'after') {
    let delimiterTokens: Token[] = []
    if (type === 'before')
      delimiterTokens = tokens.slice(0, index)
    if (type === 'after')
      delimiterTokens = tokens.slice(index, tokens.length)
    const outputToken = type === 'before' ? 'delimiter.tag.opening.liquid.html' : 'delimiter.tag.closing.liquid.html'
    const statementToken = type === 'before' ? 'delimiter.output.opening.liquid.html' : 'delimiter.output.closing.liquid.html'
    return delimiterTokens.some(token => token.type === outputToken || token.type === statementToken)
  },
  isValidTESLLocation(model: Monaco.editor.ITextModel, position: Monaco.IPosition) {
    const locationTokens = this.tokenizer(model, position)
    let valid = false
    const { column } = position

    for (let i = locationTokens.length - 1; i >= 0; i--) {
      if (column - 1 > locationTokens[i].offset) {
        // Check before and after the current column for a valid liquid delimiter
        const beforeDelimiterResults = this.checkDelimiter(locationTokens, i, 'before')
        const afterDelimiterResults = this.checkDelimiter(locationTokens, i, 'after')
        valid = beforeDelimiterResults && afterDelimiterResults
        break
      }
    }
    return valid
  },
}

export default TESLtokenizer
