import type { languages } from 'monaco-editor'
export const emailHtmlData: languages.html.HTMLDataV1 = {
  version: 1.1,
  tags: [
    {
      name: 'html',
      description: {
        kind: 'markdown',
        value: 'The html element represents the root of an HTML document.',
      },
      attributes: [
        {
          name: 'manifest',
          description: {
            kind: 'markdown',
            value: 'Specifies the URI of a resource manifest indicating resources that should be cached locally. See [Using the application cache](https://developer.mozilla.org/en-US/docs/Web/HTML/Using_the_application_cache) for details.',
          },
        },
        {
          name: 'version',
          description: 'Specifies the version of the HTML [Document Type Definition](https://developer.mozilla.org/en-US/docs/Glossary/DTD "Document Type Definition: In HTML, the doctype is the required "<!DOCTYPE html>" preamble found at the top of all documents. Its sole purpose is to prevent a browser from switching into so-called “quirks mode” when rendering a document; that is, the "<!DOCTYPE html>" doctype ensures that the browser makes a best-effort attempt at following the relevant specifications, rather than using a different rendering mode that is incompatible with some specifications.") that governs the current document. This attribute is not needed, because it is redundant with the version information in the document type declaration.',
        },
        {
          name: 'xmlns',
          description: 'Specifies the XML Namespace of the document. Default value is `"http://www.w3.org/1999/xhtml"`. This is required in documents parsed with XML parsers, and optional in text/html documents.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/html',
        },
      ],
    },
    {
      name: 'head',
      description: {
        kind: 'markdown',
        value: 'The head element represents a collection of metadata for the Document.',
      },
      attributes: [
        {
          name: 'profile',
          description: 'The URIs of one or more metadata profiles, separated by white space.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/head',
        },
      ],
    },
    {
      name: 'title',
      description: {
        kind: 'markdown',
        value: 'The title element represents the document\'s title or name. Authors should use titles that identify their documents even when they are used out of context, for example in a user\'s history or bookmarks, or in search results. The document\'s title is often different from its first heading, since the first heading does not have to stand alone when taken out of context.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/title',
        },
      ],
    },
    {
      name: 'meta',
      description: {
        kind: 'markdown',
        value: 'The meta element represents various kinds of metadata that cannot be expressed using the title, base, link, style, and script elements.',
      },
      void: true,
      attributes: [
        {
          name: 'name',
          description: {
            kind: 'markdown',
            value: 'This attribute defines the name of a piece of document-level metadata. It should not be set if one of the attributes [`itemprop`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes#attr-itemprop), [`http-equiv`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-http-equiv) or [`charset`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-charset) is also set.\n\nThis metadata name is associated with the value contained by the [`content`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-content) attribute. The possible values for the name attribute are:\n\n*   `application-name` which defines the name of the application running in the web page.\n    \n    **Note:**\n    \n    *   Browsers may use this to identify the application. It is different from the [`<title>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/title "The HTML Title element (<title>) defines the document\'s title that is shown in a browser\'s title bar or a page\'s tab.") element, which usually contain the application name, but may also contain information like the document name or a status.\n    *   Simple web pages shouldn\'t define an application-name.\n    \n*   `author` which defines the name of the document\'s author.\n*   `description` which contains a short and accurate summary of the content of the page. Several browsers, like Firefox and Opera, use this as the default description of bookmarked pages.\n*   `generator` which contains the identifier of the software that generated the page.\n*   `keywords` which contains words relevant to the page\'s content separated by commas.\n*   `referrer` which controls the [`Referer` HTTP header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer) attached to requests sent from the document:\n    \n    Values for the `content` attribute of `<meta name="referrer">`\n    \n    `no-referrer`\n    \n    Do not send a HTTP `Referrer` header.\n    \n    `origin`\n    \n    Send the [origin](https://developer.mozilla.org/en-US/docs/Glossary/Origin) of the document.\n    \n    `no-referrer-when-downgrade`\n    \n    Send the [origin](https://developer.mozilla.org/en-US/docs/Glossary/Origin) as a referrer to URLs as secure as the current page, (https→https), but does not send a referrer to less secure URLs (https→http). This is the default behaviour.\n    \n    `origin-when-cross-origin`\n    \n    Send the full URL (stripped of parameters) for same-origin requests, but only send the [origin](https://developer.mozilla.org/en-US/docs/Glossary/Origin) for other cases.\n    \n    `same-origin`\n    \n    A referrer will be sent for [same-site origins](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy), but cross-origin requests will contain no referrer information.\n    \n    `strict-origin`\n    \n    Only send the origin of the document as the referrer to a-priori as-much-secure destination (HTTPS->HTTPS), but don\'t send it to a less secure destination (HTTPS->HTTP).\n    \n    `strict-origin-when-cross-origin`\n    \n    Send a full URL when performing a same-origin request, only send the origin of the document to a-priori as-much-secure destination (HTTPS->HTTPS), and send no header to a less secure destination (HTTPS->HTTP).\n    \n    `unsafe-URL`\n    \n    Send the full URL (stripped of parameters) for same-origin or cross-origin requests.\n    \n    **Notes:**\n    \n    *   Some browsers support the deprecated values of `always`, `default`, and `never` for referrer.\n    *   Dynamically inserting `<meta name="referrer">` (with [`document.write`](https://developer.mozilla.org/en-US/docs/Web/API/Document/write) or [`appendChild`](https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild)) makes the referrer behaviour unpredictable.\n    *   When several conflicting policies are defined, the no-referrer policy is applied.\n    \n\nThis attribute may also have a value taken from the extended list defined on [WHATWG Wiki MetaExtensions page](https://wiki.whatwg.org/wiki/MetaExtensions). Although none have been formally accepted yet, a few commonly used names are:\n\n*   `creator` which defines the name of the creator of the document, such as an organization or institution. If there are more than one, several [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") elements should be used.\n*   `googlebot`, a synonym of `robots`, is only followed by Googlebot (the indexing crawler for Google).\n*   `publisher` which defines the name of the document\'s publisher.\n*   `robots` which defines the behaviour that cooperative crawlers, or "robots", should use with the page. It is a comma-separated list of the values below:\n    \n    Values for the content of `<meta name="robots">`\n    \n    Value\n    \n    Description\n    \n    Used by\n    \n    `index`\n    \n    Allows the robot to index the page (default).\n    \n    All\n    \n    `noindex`\n    \n    Requests the robot to not index the page.\n    \n    All\n    \n    `follow`\n    \n    Allows the robot to follow the links on the page (default).\n    \n    All\n    \n    `nofollow`\n    \n    Requests the robot to not follow the links on the page.\n    \n    All\n    \n    `none`\n    \n    Equivalent to `noindex, nofollow`\n    \n    [Google](https://support.google.com/webmasters/answer/79812)\n    \n    `noodp`\n    \n    Prevents using the [Open Directory Project](https://www.dmoz.org/) description, if any, as the page description in search engine results.\n    \n    [Google](https://support.google.com/webmasters/answer/35624#nodmoz), [Yahoo](https://help.yahoo.com/kb/search-for-desktop/meta-tags-robotstxt-yahoo-search-sln2213.html#cont5), [Bing](https://www.bing.com/webmaster/help/which-robots-metatags-does-bing-support-5198d240)\n    \n    `noarchive`\n    \n    Requests the search engine not to cache the page content.\n    \n    [Google](https://developers.google.com/webmasters/control-crawl-index/docs/robots_meta_tag#valid-indexing--serving-directives), [Yahoo](https://help.yahoo.com/kb/search-for-desktop/SLN2213.html), [Bing](https://www.bing.com/webmaster/help/which-robots-metatags-does-bing-support-5198d240)\n    \n    `nosnippet`\n    \n    Prevents displaying any description of the page in search engine results.\n    \n    [Google](https://developers.google.com/webmasters/control-crawl-index/docs/robots_meta_tag#valid-indexing--serving-directives), [Bing](https://www.bing.com/webmaster/help/which-robots-metatags-does-bing-support-5198d240)\n    \n    `noimageindex`\n    \n    Requests this page not to appear as the referring page of an indexed image.\n    \n    [Google](https://developers.google.com/webmasters/control-crawl-index/docs/robots_meta_tag#valid-indexing--serving-directives)\n    \n    `nocache`\n    \n    Synonym of `noarchive`.\n    \n    [Bing](https://www.bing.com/webmaster/help/which-robots-metatags-does-bing-support-5198d240)\n    \n    **Notes:**\n    \n    *   Only cooperative robots follow these rules. Do not expect to prevent e-mail harvesters with them.\n    *   The robot still needs to access the page in order to read these rules. To prevent bandwidth consumption, use a _[robots.txt](https://developer.mozilla.org/en-US/docs/Glossary/robots.txt "robots.txt: Robots.txt is a file which is usually placed in the root of any website. It decides whether crawlers are permitted or forbidden access to the web site.")_ file.\n    *   If you want to remove a page, `noindex` will work, but only after the robot visits the page again. Ensure that the `robots.txt` file is not preventing revisits.\n    *   Some values are mutually exclusive, like `index` and `noindex`, or `follow` and `nofollow`. In these cases the robot\'s behaviour is undefined and may vary between them.\n    *   Some crawler robots, like Google, Yahoo and Bing, support the same values for the HTTP header `X-Robots-Tag`; this allows non-HTML documents like images to use these rules.\n    \n*   `slurp`, is a synonym of `robots`, but only for Slurp - the crawler for Yahoo Search.\n*   `viewport`, which gives hints about the size of the initial size of the [viewport](https://developer.mozilla.org/en-US/docs/Glossary/viewport "viewport: A viewport represents a polygonal (normally rectangular) area in computer graphics that is currently being viewed. In web browser terms, it refers to the part of the document you\'re viewing which is currently visible in its window (or the screen, if the document is being viewed in full screen mode). Content outside the viewport is not visible onscreen until scrolled into view."). Used by mobile devices only.\n    \n    Values for the content of `<meta name="viewport">`\n    \n    Value\n    \n    Possible subvalues\n    \n    Description\n    \n    `width`\n    \n    A positive integer number, or the text `device-width`\n    \n    Defines the pixel width of the viewport that you want the web site to be rendered at.\n    \n    `height`\n    \n    A positive integer, or the text `device-height`\n    \n    Defines the height of the viewport. Not used by any browser.\n    \n    `initial-scale`\n    \n    A positive number between `0.0` and `10.0`\n    \n    Defines the ratio between the device width (`device-width` in portrait mode or `device-height` in landscape mode) and the viewport size.\n    \n    `maximum-scale`\n    \n    A positive number between `0.0` and `10.0`\n    \n    Defines the maximum amount to zoom in. It must be greater or equal to the `minimum-scale` or the behaviour is undefined. Browser settings can ignore this rule and iOS10+ ignores it by default.\n    \n    `minimum-scale`\n    \n    A positive number between `0.0` and `10.0`\n    \n    Defines the minimum zoom level. It must be smaller or equal to the `maximum-scale` or the behaviour is undefined. Browser settings can ignore this rule and iOS10+ ignores it by default.\n    \n    `user-scalable`\n    \n    `yes` or `no`\n    \n    If set to `no`, the user is not able to zoom in the webpage. The default is `yes`. Browser settings can ignore this rule, and iOS10+ ignores it by default.\n    \n    Specification\n    \n    Status\n    \n    Comment\n    \n    [CSS Device Adaptation  \n    The definition of \'<meta name="viewport">\' in that specification.](https://drafts.csswg.org/css-device-adapt/#viewport-meta)\n    \n    Working Draft\n    \n    Non-normatively describes the Viewport META element\n    \n    See also: [`@viewport`](https://developer.mozilla.org/en-US/docs/Web/CSS/@viewport "The @viewport CSS at-rule lets you configure the viewport through which the document is viewed. It\'s primarily used for mobile devices, but is also used by desktop browsers that support features like "snap to edge" (such as Microsoft Edge).")\n    \n    **Notes:**\n    \n    *   Though unstandardized, this declaration is respected by most mobile browsers due to de-facto dominance.\n    *   The default values may vary between devices and browsers.\n    *   To learn about this declaration in Firefox for Mobile, see [this article](https://developer.mozilla.org/en-US/docs/Mobile/Viewport_meta_tag "Mobile/Viewport meta tag").',
          },
        },
        {
          name: 'http-equiv',
          description: {
            kind: 'markdown',
            value: 'Defines a pragma directive. The attribute is named `**http-equiv**(alent)` because all the allowed values are names of particular HTTP headers:\n\n*   `"content-language"`  \n    Defines the default language of the page. It can be overridden by the [lang](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/lang) attribute on any element.\n    \n    **Warning:** Do not use this value, as it is obsolete. Prefer the `lang` attribute on the [`<html>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/html "The HTML <html> element represents the root (top-level element) of an HTML document, so it is also referred to as the root element. All other elements must be descendants of this element.") element.\n    \n*   `"content-security-policy"`  \n    Allows page authors to define a [content policy](https://developer.mozilla.org/en-US/docs/Web/Security/CSP/CSP_policy_directives) for the current page. Content policies mostly specify allowed server origins and script endpoints which help guard against cross-site scripting attacks.\n*   `"content-type"`  \n    Defines the [MIME type](https://developer.mozilla.org/en-US/docs/Glossary/MIME_type) of the document, followed by its character encoding. It follows the same syntax as the HTTP `content-type` entity-header field, but as it is inside a HTML page, most values other than `text/html` are impossible. Therefore the valid syntax for its `content` is the string \'`text/html`\' followed by a character set with the following syntax: \'`; charset=_IANAcharset_`\', where `IANAcharset` is the _preferred MIME name_ for a character set as [defined by the IANA.](https://www.iana.org/assignments/character-sets)\n    \n    **Warning:** Do not use this value, as it is obsolete. Use the [`charset`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-charset) attribute on the [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") element.\n    \n    **Note:** As [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") can\'t change documents\' types in XHTML or HTML5\'s XHTML serialization, never set the MIME type to an XHTML MIME type with `<meta>`.\n    \n*   `"refresh"`  \n    This instruction specifies:\n    *   The number of seconds until the page should be reloaded - only if the [`content`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-content) attribute contains a positive integer.\n    *   The number of seconds until the page should redirect to another - only if the [`content`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-content) attribute contains a positive integer followed by the string \'`;url=`\', and a valid URL.\n*   `"set-cookie"`  \n    Defines a [cookie](https://developer.mozilla.org/en-US/docs/cookie) for the page. Its content must follow the syntax defined in the [IETF HTTP Cookie Specification](https://tools.ietf.org/html/draft-ietf-httpstate-cookie-14).\n    \n    **Warning:** Do not use this instruction, as it is obsolete. Use the HTTP header [`Set-Cookie`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie) instead.',
          },
        },
        {
          name: 'content',
          description: {
            kind: 'markdown',
            value: 'This attribute contains the value for the [`http-equiv`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-http-equiv) or [`name`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-name) attribute, depending on which is used.',
          },
        },
        {
          name: 'charset',
          description: {
            kind: 'markdown',
            value: 'This attribute declares the page\'s character encoding. It must contain a [standard IANA MIME name for character encodings](https://www.iana.org/assignments/character-sets). Although the standard doesn\'t request a specific encoding, it suggests:\n\n*   Authors are encouraged to use [`UTF-8`](https://developer.mozilla.org/en-US/docs/Glossary/UTF-8).\n*   Authors should not use ASCII-incompatible encodings to avoid security risk: browsers not supporting them may interpret harmful content as HTML. This happens with the `JIS_C6226-1983`, `JIS_X0212-1990`, `HZ-GB-2312`, `JOHAB`, the ISO-2022 family and the EBCDIC family.\n\n**Note:** ASCII-incompatible encodings are those that don\'t map the 8-bit code points `0x20` to `0x7E` to the `0x0020` to `0x007E` Unicode code points)\n\n*   Authors **must not** use `CESU-8`, `UTF-7`, `BOCU-1` and/or `SCSU` as [cross-site scripting](https://developer.mozilla.org/en-US/docs/Glossary/Cross-site_scripting) attacks with these encodings have been demonstrated.\n*   Authors should not use `UTF-32` because not all HTML5 encoding algorithms can distinguish it from `UTF-16`.\n\n**Notes:**\n\n*   The declared character encoding must match the one the page was saved with to avoid garbled characters and security holes.\n*   The [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") element declaring the encoding must be inside the [`<head>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/head "The HTML <head> element provides general information (metadata) about the document, including its title and links to its scripts and style sheets.") element and **within the first 1024 bytes** of the HTML as some browsers only look at those bytes before choosing an encoding.\n*   This [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") element is only one part of the [algorithm to determine a page\'s character set](https://www.whatwg.org/specs/web-apps/current-work/multipage/parsing.html#encoding-sniffing-algorithm "Algorithm charset page"). The [`Content-Type` header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type) and any [Byte-Order Marks](https://developer.mozilla.org/en-US/docs/Glossary/Byte-Order_Mark "The definition of that term (Byte-Order Marks) has not been written yet; please consider contributing it!") override this element.\n*   It is strongly recommended to define the character encoding. If a page\'s encoding is undefined, cross-scripting techniques are possible, such as the [`UTF-7` fallback cross-scripting technique](https://code.google.com/p/doctype-mirror/wiki/ArticleUtf7).\n*   The [`<meta>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta "The HTML <meta> element represents metadata that cannot be represented by other HTML meta-related elements, like <base>, <link>, <script>, <style> or <title>.") element with a `charset` attribute is a synonym for the pre-HTML5 `<meta http-equiv="Content-Type" content="text/html; charset=_IANAcharset_">`, where _`IANAcharset`_ contains the value of the equivalent [`charset`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-charset) attribute. This syntax is still allowed, although no longer recommended.',
          },
        },
        {
          name: 'scheme',
          description: 'This attribute defines the scheme in which metadata is described. A scheme is a context leading to the correct interpretations of the [`content`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-content) value, like a format.\n\n**Warning:** Do not use this value, as it is obsolete. There is no replacement as there was no real usage for it.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/meta',
        },
      ],
    },
    {
      name: 'style',
      description: {
        kind: 'markdown',
        value: 'The style element allows authors to embed style information in their documents. The style element is one of several inputs to the styling processing model. The element does not represent content for the user.',
      },
      attributes: [
        {
          name: 'media',
          description: {
            kind: 'markdown',
            value: 'This attribute defines which media the style should be applied to. Its value is a [media query](https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Media_queries), which defaults to `all` if the attribute is missing.',
          },
        },
        {
          name: 'type',
          description: {
            kind: 'markdown',
            value: 'This attribute defines the styling language as a MIME type (charset should not be specified). This attribute is optional and defaults to `text/css` if it is not specified — there is very little reason to include this in modern web documents.',
          },
        },
        {
          name: 'scoped',
          valueSet: 'v',
        },
        {
          name: 'title',
          description: 'This attribute specifies [alternative style sheet](https://developer.mozilla.org/en-US/docs/Web/CSS/Alternative_style_sheets) sets.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/style',
        },
      ],
    },
    {
      name: 'body',
      description: {
        kind: 'markdown',
        value: 'The body element represents the content of the document.',
      },
      attributes: [
        {
          name: 'alink',
          description: 'Color of text for hyperlinks when selected. _This method is non-conforming, use CSS [`color`](https://developer.mozilla.org/en-US/docs/Web/CSS/color "The color CSS property sets the foreground color value of an element\'s text and text decorations, and sets the currentcolor value.") property in conjunction with the [`:active`](https://developer.mozilla.org/en-US/docs/Web/CSS/:active "The :active CSS pseudo-class represents an element (such as a button) that is being activated by the user.") pseudo-class instead._',
        },
        {
          name: 'background',
          description: 'URI of a image to use as a background. _This method is non-conforming, use CSS [`background`](https://developer.mozilla.org/en-US/docs/Web/CSS/background "The background shorthand CSS property sets all background style properties at once, such as color, image, origin and size, or repeat method.") property on the element instead._',
        },
        {
          name: 'bgcolor',
          description: 'Background color for the document. _This method is non-conforming, use CSS [`background-color`](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color "The background-color CSS property sets the background color of an element.") property on the element instead._',
        },
        {
          name: 'bottommargin',
          description: 'The margin of the bottom of the body. _This method is non-conforming, use CSS [`margin-bottom`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom "The margin-bottom CSS property sets the margin area on the bottom of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") property on the element instead._',
        },
        {
          name: 'leftmargin',
          description: 'The margin of the left of the body. _This method is non-conforming, use CSS [`margin-left`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left "The margin-left CSS property sets the margin area on the left side of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") property on the element instead._',
        },
        {
          name: 'link',
          description: 'Color of text for unvisited hypertext links. _This method is non-conforming, use CSS [`color`](https://developer.mozilla.org/en-US/docs/Web/CSS/color "The color CSS property sets the foreground color value of an element\'s text and text decorations, and sets the currentcolor value.") property in conjunction with the [`:link`](https://developer.mozilla.org/en-US/docs/Web/CSS/:link "The :link CSS pseudo-class represents an element that has not yet been visited. It matches every unvisited <a>, <area>, or <link> element that has an href attribute.") pseudo-class instead._',
        },
        {
          name: 'rightmargin',
          description: 'The margin of the right of the body. _This method is non-conforming, use CSS [`margin-right`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right "The margin-right CSS property sets the margin area on the right side of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") property on the element instead._',
        },
        {
          name: 'text',
          description: 'Foreground color of text. _This method is non-conforming, use CSS [`color`](https://developer.mozilla.org/en-US/docs/Web/CSS/color "The color CSS property sets the foreground color value of an element\'s text and text decorations, and sets the currentcolor value.") property on the element instead._',
        },
        {
          name: 'topmargin',
          description: 'The margin of the top of the body. _This method is non-conforming, use CSS [`margin-top`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top "The margin-top CSS property sets the margin area on the top of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") property on the element instead._',
        },
        {
          name: 'vlink',
          description: 'Color of text for visited hypertext links. _This method is non-conforming, use CSS [`color`](https://developer.mozilla.org/en-US/docs/Web/CSS/color "The color CSS property sets the foreground color value of an element\'s text and text decorations, and sets the currentcolor value.") property in conjunction with the [`:visited`](https://developer.mozilla.org/en-US/docs/Web/CSS/:visited "The :visited CSS pseudo-class represents links that the user has already visited. For privacy reasons, the styles that can be modified using this selector are very limited.") pseudo-class instead._',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/body',
        },
      ],
    },
    {
      name: 'h1',
      description: {
        kind: 'markdown',
        value: 'The h1 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'h2',
      description: {
        kind: 'markdown',
        value: 'The h2 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'h3',
      description: {
        kind: 'markdown',
        value: 'The h3 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'h4',
      description: {
        kind: 'markdown',
        value: 'The h4 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'h5',
      description: {
        kind: 'markdown',
        value: 'The h5 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'h6',
      description: {
        kind: 'markdown',
        value: 'The h6 element represents a section heading.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/Heading_Elements',
        },
      ],
    },
    {
      name: 'address',
      description: {
        kind: 'markdown',
        value: 'The address element represents the contact information for its nearest article or body element ancestor. If that is the body element, then the contact information applies to the document as a whole.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/address',
        },
      ],
    },
    {
      name: 'p',
      description: {
        kind: 'markdown',
        value: 'The p element represents a paragraph.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/p',
        },
      ],
    },
    {
      name: 'hr',
      description: {
        kind: 'markdown',
        value: 'The hr element represents a paragraph-level thematic break, e.g. a scene change in a story, or a transition to another topic within a section of a reference book.',
      },
      void: true,
      attributes: [
        {
          name: 'align',
          description: 'Sets the alignment of the rule on the page. If no value is specified, the default value is `left`.',
        },
        {
          name: 'color',
          description: 'Sets the color of the rule through color name or hexadecimal value.',
        },
        {
          name: 'noshade',
          description: 'Sets the rule to have no shading.',
        },
        {
          name: 'size',
          description: 'Sets the height, in pixels, of the rule.',
        },
        {
          name: 'width',
          description: 'Sets the length of the rule on the page through a pixel or percentage value.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/hr',
        },
      ],
    },
    {
      name: 'blockquote',
      description: {
        kind: 'markdown',
        value: 'The blockquote element represents content that is quoted from another source, optionally with a citation which must be within a footer or cite element, and optionally with in-line changes such as annotations and abbreviations.',
      },
      attributes: [
        {
          name: 'cite',
          description: {
            kind: 'markdown',
            value: 'A URL that designates a source document or message for the information quoted. This attribute is intended to point to information explaining the context or the reference for the quote.',
          },
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/blockquote',
        },
      ],
    },
    {
      name: 'ol',
      description: {
        kind: 'markdown',
        value: 'The ol element represents a list of items, where the items have been intentionally ordered, such that changing the order would change the meaning of the document.',
      },
      attributes: [
        {
          name: 'reversed',
          valueSet: 'v',
          description: {
            kind: 'markdown',
            value: 'This Boolean attribute specifies that the items of the list are specified in reversed order.',
          },
        },
        {
          name: 'start',
          description: {
            kind: 'markdown',
            value: 'This integer attribute specifies the start value for numbering the individual list items. Although the ordering type of list elements might be Roman numerals, such as XXXI, or letters, the value of start is always represented as a number. To start numbering elements from the letter "C", use `<ol start="3">`.\n\n**Note**: This attribute was deprecated in HTML4, but reintroduced in HTML5.',
          },
        },
        {
          name: 'type',
          valueSet: 'lt',
          description: {
            kind: 'markdown',
            value: 'Indicates the numbering type:\n\n*   `\'a\'` indicates lowercase letters,\n*   `\'A\'` indicates uppercase letters,\n*   `\'i\'` indicates lowercase Roman numerals,\n*   `\'I\'` indicates uppercase Roman numerals,\n*   and `\'1\'` indicates numbers (default).\n\nThe type set is used for the entire list unless a different [`type`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li#attr-type) attribute is used within an enclosed [`<li>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li "The HTML <li> element is used to represent an item in a list. It must be contained in a parent element: an ordered list (<ol>), an unordered list (<ul>), or a menu (<menu>). In menus and unordered lists, list items are usually displayed using bullet points. In ordered lists, they are usually displayed with an ascending counter on the left, such as a number or letter.") element.\n\n**Note:** This attribute was deprecated in HTML4, but reintroduced in HTML5.\n\nUnless the value of the list number matters (e.g. in legal or technical documents where items are to be referenced by their number/letter), the CSS [`list-style-type`](https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type "The list-style-type CSS property sets the marker (such as a disc, character, or custom counter style) of a list item element.") property should be used instead.',
          },
        },
        {
          name: 'compact',
          description: 'This Boolean attribute hints that the list should be rendered in a compact style. The interpretation of this attribute depends on the user agent and it doesn\'t work in all browsers.\n\n**Warning:** Do not use this attribute, as it has been deprecated: the [`<ol>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol "The HTML <ol> element represents an ordered list of items, typically rendered as a numbered list.") element should be styled using [CSS](https://developer.mozilla.org/en-US/docs/CSS). To give an effect similar to the `compact` attribute, the [CSS](https://developer.mozilla.org/en-US/docs/CSS) property [`line-height`](https://developer.mozilla.org/en-US/docs/Web/CSS/line-height "The line-height CSS property sets the amount of space used for lines, such as in text. On block-level elements, it specifies the minimum height of line boxes within the element. On non-replaced inline elements, it specifies the height that is used to calculate line box height.") can be used with a value of `80%`.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/ol',
        },
      ],
    },
    {
      name: 'ul',
      description: {
        kind: 'markdown',
        value: 'The ul element represents a list of items, where the order of the items is not important — that is, where changing the order would not materially change the meaning of the document.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/ul',
        },
      ],
    },
    {
      name: 'li',
      description: {
        kind: 'markdown',
        value: 'The li element represents a list item. If its parent element is an ol, ul, or menu element, then the element is an item of the parent element\'s list, as defined for those elements. Otherwise, the list item has no defined list-related relationship to any other li element.',
      },
      attributes: [
        {
          name: 'value',
          description: {
            kind: 'markdown',
            value: 'This integer attribute indicates the current ordinal value of the list item as defined by the [`<ol>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol "The HTML <ol> element represents an ordered list of items, typically rendered as a numbered list.") element. The only allowed value for this attribute is a number, even if the list is displayed with Roman numerals or letters. List items that follow this one continue numbering from the value set. The **value** attribute has no meaning for unordered lists ([`<ul>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul "The HTML <ul> element represents an unordered list of items, typically rendered as a bulleted list.")) or for menus ([`<menu>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/menu "The HTML <menu> element represents a group of commands that a user can perform or activate. This includes both list menus, which might appear across the top of a screen, as well as context menus, such as those that might appear underneath a button after it has been clicked.")).\n\n**Note**: This attribute was deprecated in HTML4, but reintroduced in HTML5.\n\n**Note:** Prior to Gecko 9.0, negative values were incorrectly converted to 0. Starting in Gecko 9.0 all integer values are correctly parsed.',
          },
        },
        {
          name: 'type',
          description: 'This character attribute indicates the numbering type:\n\n*   `a`: lowercase letters\n*   `A`: uppercase letters\n*   `i`: lowercase Roman numerals\n*   `I`: uppercase Roman numerals\n*   `1`: numbers\n\nThis type overrides the one used by its parent [`<ol>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol "The HTML <ol> element represents an ordered list of items, typically rendered as a numbered list.") element, if any.\n\n**Usage note:** This attribute has been deprecated: use the CSS [`list-style-type`](https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type "The list-style-type CSS property sets the marker (such as a disc, character, or custom counter style) of a list item element.") property instead.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/li',
        },
      ],
    },
    {
      name: 'dl',
      description: {
        kind: 'markdown',
        value: 'The dl element represents an association list consisting of zero or more name-value groups (a description list). A name-value group consists of one or more names (dt elements) followed by one or more values (dd elements), ignoring any nodes other than dt and dd elements. Within a single dl element, there should not be more than one dt element for each name.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/dl',
        },
      ],
    },
    {
      name: 'dt',
      description: {
        kind: 'markdown',
        value: 'The dt element represents the term, or name, part of a term-description group in a description list (dl element).',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/dt',
        },
      ],
    },
    {
      name: 'dd',
      description: {
        kind: 'markdown',
        value: 'The dd element represents the description, definition, or value, part of a term-description group in a description list (dl element).',
      },
      attributes: [
        {
          name: 'nowrap',
          description: 'If the value of this attribute is set to `yes`, the definition text will not wrap. The default value is `no`.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/dd',
        },
      ],
    },
    {
      name: 'div',
      description: {
        kind: 'markdown',
        value: 'The div element has no special meaning at all. It represents its children. It can be used with the class, lang, and title attributes to mark up semantics common to a group of consecutive elements.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/div',
        },
      ],
    },
    {
      name: 'a',
      description: {
        kind: 'markdown',
        value: 'If the a element has an href attribute, then it represents a hyperlink (a hypertext anchor) labeled by its contents.',
      },
      attributes: [
        {
          name: 'href',
          description: {
            kind: 'markdown',
            value: 'Contains a URL or a URL fragment that the hyperlink points to.\nA URL fragment is a name preceded by a hash mark (`#`), which specifies an internal target location (an [`id`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes#attr-id) of an HTML element) within the current document. URLs are not restricted to Web (HTTP)-based documents, but can use any protocol supported by the browser. For example, [`file:`](https://en.wikipedia.org/wiki/File_URI_scheme), `ftp:`, and `mailto:` work in most browsers.\n\n**Note:** You can use `href="#top"` or the empty fragment `href="#"` to link to the top of the current page. [This behavior is specified by HTML5](https://www.w3.org/TR/html5/single-page.html#scroll-to-fragid).',
          },
        },
        {
          name: 'target',
          valueSet: 'target',
          description: {
            kind: 'markdown',
            value: 'Specifies where to display the linked URL. It is a name of, or keyword for, a _browsing context_: a tab, window, or `<iframe>`. The following keywords have special meanings:\n\n*   `_self`: Load the URL into the same browsing context as the current one. This is the default behavior.\n*   `_blank`: Load the URL into a new browsing context. This is usually a tab, but users can configure browsers to use new windows instead.\n*   `_parent`: Load the URL into the parent browsing context of the current one. If there is no parent, this behaves the same way as `_self`.\n*   `_top`: Load the URL into the top-level browsing context (that is, the "highest" browsing context that is an ancestor of the current one, and has no parent). If there is no parent, this behaves the same way as `_self`.\n\n**Note:** When using `target`, consider adding `rel="noreferrer"` to avoid exploitation of the `window.opener` API.\n\n**Note:** Linking to another page using `target="_blank"` will run the new page on the same process as your page. If the new page is executing expensive JS, your page\'s performance may suffer. To avoid this use `rel="noopener"`.',
          },
        },
        {
          name: 'rel',
          description: {
            kind: 'markdown',
            value: 'Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).',
          },
        },
        {
          name: 'hreflang',
          description: {
            kind: 'markdown',
            value: 'This attribute indicates the human language of the linked resource. It is purely advisory, with no built-in functionality. Allowed values are determined by [BCP47](https://www.ietf.org/rfc/bcp/bcp47.txt "Tags for Identifying Languages").',
          },
        },
        {
          name: 'type',
          description: {
            kind: 'markdown',
            value: 'Specifies the media type in the form of a [MIME type](https://developer.mozilla.org/en-US/docs/Glossary/MIME_type "MIME type: A MIME type (now properly called "media type", but also sometimes "content type") is a string sent along with a file indicating the type of the file (describing the content format, for example, a sound file might be labeled audio/ogg, or an image file image/png).") for the linked URL. It is purely advisory, with no built-in functionality.',
          },
        },
        {
          name: 'referrerpolicy',
          description: 'Indicates which [referrer](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer) to send when fetching the URL:\n\n*   `\'no-referrer\'` means the `Referer:` header will not be sent.\n*   `\'no-referrer-when-downgrade\'` means no `Referer:` header will be sent when navigating to an origin without HTTPS. This is the default behavior.\n*   `\'origin\'` means the referrer will be the [origin](https://developer.mozilla.org/en-US/docs/Glossary/Origin) of the page, not including information after the domain.\n*   `\'origin-when-cross-origin\'` meaning that navigations to other origins will be limited to the scheme, the host and the port, while navigations on the same origin will include the referrer\'s path.\n*   `\'strict-origin-when-cross-origin\'`\n*   `\'unsafe-url\'` means the referrer will include the origin and path, but not the fragment, password, or username. This is unsafe because it can leak data from secure URLs to insecure ones.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/a',
        },
      ],
    },
    {
      name: 'em',
      description: {
        kind: 'markdown',
        value: 'The em element represents stress emphasis of its contents.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/em',
        },
      ],
    },
    {
      name: 'strong',
      description: {
        kind: 'markdown',
        value: 'The strong element represents strong importance, seriousness, or urgency for its contents.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/strong',
        },
      ],
    },
    {
      name: 'small',
      description: {
        kind: 'markdown',
        value: 'The small element represents side comments such as small print.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/small',
        },
      ],
    },
    {
      name: 's',
      description: {
        kind: 'markdown',
        value: 'The s element represents contents that are no longer accurate or no longer relevant.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/s',
        },
      ],
    },
    {
      name: 'cite',
      description: {
        kind: 'markdown',
        value: 'The cite element represents a reference to a creative work. It must include the title of the work or the name of the author(person, people or organization) or an URL reference, or a reference in abbreviated form as per the conventions used for the addition of citation metadata.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/cite',
        },
      ],
    },
    {
      name: 'q',
      description: {
        kind: 'markdown',
        value: 'The q element represents some phrasing content quoted from another source.',
      },
      attributes: [
        {
          name: 'cite',
          description: {
            kind: 'markdown',
            value: 'The value of this attribute is a URL that designates a source document or message for the information quoted. This attribute is intended to point to information explaining the context or the reference for the quote.',
          },
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/q',
        },
      ],
    },
    {
      name: 'abbr',
      description: {
        kind: 'markdown',
        value: 'The abbr element represents an abbreviation or acronym, optionally with its expansion. The title attribute may be used to provide an expansion of the abbreviation. The attribute, if specified, must contain an expansion of the abbreviation, and nothing else.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/abbr',
        },
      ],
    },
    {
      name: 'sub',
      description: {
        kind: 'markdown',
        value: 'The sub element represents a subscript.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/sub',
        },
      ],
    },
    {
      name: 'sup',
      description: {
        kind: 'markdown',
        value: 'The sup element represents a superscript.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/sup',
        },
      ],
    },
    {
      name: 'i',
      description: {
        kind: 'markdown',
        value: 'The i element represents a span of text in an alternate voice or mood, or otherwise offset from the normal prose in a manner indicating a different quality of text, such as a taxonomic designation, a technical term, an idiomatic phrase from another language, transliteration, a thought, or a ship name in Western texts.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/i',
        },
      ],
    },
    {
      name: 'b',
      description: {
        kind: 'markdown',
        value: 'The b element represents a span of text to which attention is being drawn for utilitarian purposes without conveying any extra importance and with no implication of an alternate voice or mood, such as key words in a document abstract, product names in a review, actionable words in interactive text-driven software, or an article lede.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/b',
        },
      ],
    },
    {
      name: 'u',
      description: {
        kind: 'markdown',
        value: 'The u element represents a span of text with an unarticulated, though explicitly rendered, non-textual annotation, such as labeling the text as being a proper name in Chinese text (a Chinese proper name mark), or labeling the text as being misspelt.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/u',
        },
      ],
    },
    {
      name: 'span',
      description: {
        kind: 'markdown',
        value: 'The span element doesn\'t mean anything on its own, but can be useful when used together with the global attributes, e.g. class, lang, or dir. It represents its children.',
      },
      attributes: [],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/span',
        },
      ],
    },
    {
      name: 'br',
      description: {
        kind: 'markdown',
        value: 'The br element represents a line break.',
      },
      void: true,
      attributes: [
        {
          name: 'clear',
          description: 'Indicates where to begin the next line after the break.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/br',
        },
      ],
    },
    {
      name: 'del',
      description: {
        kind: 'markdown',
        value: 'The del element represents a removal from the document.',
      },
      attributes: [
        {
          name: 'cite',
          description: {
            kind: 'markdown',
            value: 'A URI for a resource that explains the change (for example, meeting minutes).',
          },
        },
        {
          name: 'datetime',
          description: {
            kind: 'markdown',
            value: 'This attribute indicates the time and date of the change and must be a valid date string with an optional time. If the value cannot be parsed as a date with an optional time string, the element does not have an associated time stamp. For the format of the string without a time, see [Format of a valid date string](https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats#Format_of_a_valid_date_string "Certain HTML elements use date and/or time values. The formats of the strings that specify these are described in this article.") in [Date and time formats used in HTML](https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats "Certain HTML elements use date and/or time values. The formats of the strings that specify these are described in this article."). The format of the string if it includes both date and time is covered in [Format of a valid local date and time string](https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats#Format_of_a_valid_local_date_and_time_string "Certain HTML elements use date and/or time values. The formats of the strings that specify these are described in this article.") in [Date and time formats used in HTML](https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats "Certain HTML elements use date and/or time values. The formats of the strings that specify these are described in this article.").',
          },
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/del',
        },
      ],
    },
    {
      name: 'img',
      description: {
        kind: 'markdown',
        value: 'An img element represents an image.',
      },
      void: true,
      attributes: [
        {
          name: 'alt',
          description: {
            kind: 'markdown',
            value: 'This attribute defines an alternative text description of the image.\n\n**Note:** Browsers do not always display the image referenced by the element. This is the case for non-graphical browsers (including those used by people with visual impairments), if the user chooses not to display images, or if the browser cannot display the image because it is invalid or an [unsupported type](#Supported_image_formats). In these cases, the browser may replace the image with the text defined in this element\'s `alt` attribute. You should, for these reasons and others, provide a useful value for `alt` whenever possible.\n\n**Note:** Omitting this attribute altogether indicates that the image is a key part of the content, and no textual equivalent is available. Setting this attribute to an empty string (`alt=""`) indicates that this image is _not_ a key part of the content (decorative), and that non-visual browsers may omit it from rendering.',
          },
        },
        {
          name: 'src',
          description: {
            kind: 'markdown',
            value: 'The image URL. This attribute is mandatory for the `<img>` element. On browsers supporting `srcset`, `src` is treated like a candidate image with a pixel density descriptor `1x` unless an image with this pixel density descriptor is already defined in `srcset,` or unless `srcset` contains \'`w`\' descriptors.',
          },
        },
        {
          name: 'crossorigin',
          valueSet: 'xo',
          description: {
            kind: 'markdown',
            value: 'This enumerated attribute indicates if the fetching of the related image must be done using CORS or not. [CORS-enabled images](https://developer.mozilla.org/en-US/docs/CORS_Enabled_Image) can be reused in the [`<canvas>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/canvas "Use the HTML <canvas> element with either the canvas scripting API or the WebGL API to draw graphics and animations.") element without being "[tainted](https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image#What_is_a_tainted_canvas)." The allowed values are:\n`anonymous`\n\nA cross-origin request (i.e., with `Origin:` HTTP header) is performed, but no credential is sent (i.e., no cookie, X.509 certificate, or HTTP Basic authentication). If the server does not give credentials to the origin site (by not setting the [`Access-Control-Allow-Origin`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin "The Access-Control-Allow-Origin response header indicates whether the response can be shared with requesting code from the given origin.") HTTP header), the image will be tainted and its usage restricted.\n\n`use-credentials`\n\nA cross-origin request (i.e., with the [`Origin`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Origin "The Origin request header indicates where a fetch originates from. It doesn\'t include any path information, but only the server name. It is sent with CORS requests, as well as with POST requests. It is similar to the Referer header, but, unlike this header, it doesn\'t disclose the whole path.") HTTP header) performed along with credentials sent (i.e., a cookie, certificate, or HTTP Basic authentication). If the server does not give credentials to the origin site (through the `Access-Control-Allow-Credentials` HTTP header), the image will be tainted and its usage restricted.\n\nIf the attribute is not present, the resource is fetched without a CORS request (i.e., without sending the `Origin` HTTP header), preventing its non-tainted usage in [`<canvas>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/canvas "Use the HTML <canvas> element with either the canvas scripting API or the WebGL API to draw graphics and animations.") elements. If invalid, it is handled as if the `anonymous` value was used. See [CORS settings attributes](https://developer.mozilla.org/en-US/docs/HTML/CORS_settings_attributes) for additional information.',
          },
        },
        {
          name: 'usemap',
          description: {
            kind: 'markdown',
            value: 'The partial URL (starting with \'#\') of an [image map](https://developer.mozilla.org/en-US/docs/HTML/Element/map) associated with the element.\n\n**Note:** You cannot use this attribute if the `<img>` element is a descendant of an [`<a>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a "The HTML <a> element (or anchor element) creates a hyperlink to other web pages, files, locations within the same page, email addresses, or any other URL.") or [`<button>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button "The HTML <button> element represents a clickable button, which can be used in forms or anywhere in a document that needs simple, standard button functionality.") element.',
          },
        },
        {
          name: 'ismap',
          valueSet: 'v',
          description: {
            kind: 'markdown',
            value: 'This Boolean attribute indicates that the image is part of a server-side map. If so, the precise coordinates of a click are sent to the server.\n\n**Note:** This attribute is allowed only if the `<img>` element is a descendant of an [`<a>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a "The HTML <a> element (or anchor element) creates a hyperlink to other web pages, files, locations within the same page, email addresses, or any other URL.") element with a valid [`href`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#attr-href) attribute.',
          },
        },
        {
          name: 'width',
          description: {
            kind: 'markdown',
            value: 'The intrinsic width of the image in pixels.',
          },
        },
        {
          name: 'height',
          description: {
            kind: 'markdown',
            value: 'The intrinsic height of the image in pixels.',
          },
        },
        {
          name: 'decoding',
          valueSet: 'decoding',
          description: {
            kind: 'markdown',
            value: 'Provides an image decoding hint to the browser. The allowed values are:\n`sync`\n\nDecode the image synchronously for atomic presentation with other content.\n\n`async`\n\nDecode the image asynchronously to reduce delay in presenting other content.\n\n`auto`\n\nDefault mode, which indicates no preference for the decoding mode. The browser decides what is best for the user.',
          },
        },
        {
          name: 'loading',
          valueSet: 'loading',
        },
        {
          name: 'referrerpolicy',
          valueSet: 'referrerpolicy',
          description: {
            kind: 'markdown',
            value: 'A string indicating which referrer to use when fetching the resource:\n\n*   `no-referrer:` The [`Referer`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer "The Referer request header contains the address of the previous web page from which a link to the currently requested page was followed. The Referer header allows servers to identify where people are visiting them from and may use that data for analytics, logging, or optimized caching, for example.") header will not be sent.\n*   `no-referrer-when-downgrade:` No `Referer` header will be sent when navigating to an origin without TLS (HTTPS). This is a user agent’s default behavior if no policy is otherwise specified.\n*   `origin:` The `Referer` header will include the page of origin\'s scheme, the host, and the port.\n*   `origin-when-cross-origin:` Navigating to other origins will limit the included referral data to the scheme, the host and the port, while navigating from the same origin will include the referrer\'s full path.\n*   `unsafe-url:` The `Referer` header will include the origin and the path, but not the fragment, password, or username. This case is unsafe because it can leak origins and paths from TLS-protected resources to insecure origins.',
          },
        },
        {
          name: 'importance',
          description: 'Indicates the relative importance of the resource. Priority hints are delegated using the values:',
        },
        {
          name: 'importance',
          description: '`auto`: Indicates **no preference**. The browser may use its own heuristics to decide the priority of the image.\n\n`high`: Indicates to the browser that the image is of **high** priority.\n\n`low`: Indicates to the browser that the image is of **low** priority.',
        },
        {
          name: 'intrinsicsize',
          description: 'This attribute tells the browser to ignore the actual intrinsic size of the image and pretend it’s the size specified in the attribute. Specifically, the image would raster at these dimensions and `naturalWidth`/`naturalHeight` on images would return the values specified in this attribute. [Explainer](https://github.com/ojanvafai/intrinsicsize-attribute), [examples](https://googlechrome.github.io/samples/intrinsic-size/index.html)',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/img',
        },
      ],
    },
    {
      name: 'map',
      description: {
        kind: 'markdown',
        value: 'The map element, in conjunction with an img element and any area element descendants, defines an image map. The element represents its children.',
      },
      attributes: [
        {
          name: 'name',
          description: {
            kind: 'markdown',
            value: 'The name attribute gives the map a name so that it can be referenced. The attribute must be present and must have a non-empty value with no space characters. The value of the name attribute must not be a compatibility-caseless match for the value of the name attribute of another map element in the same document. If the id attribute is also specified, both attributes must have the same value.',
          },
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/map',
        },
      ],
    },
    {
      name: 'area',
      description: {
        kind: 'markdown',
        value: 'The area element represents either a hyperlink with some text and a corresponding area on an image map, or a dead area on an image map.',
      },
      void: true,
      attributes: [
        {
          name: 'alt',
        },
        {
          name: 'coords',
        },
        {
          name: 'shape',
          valueSet: 'sh',
        },
        {
          name: 'href',
        },
        {
          name: 'target',
          valueSet: 'target',
        },
        {
          name: 'download',
        },
        {
          name: 'ping',
        },
        {
          name: 'rel',
        },
        {
          name: 'hreflang',
        },
        {
          name: 'type',
        },
        {
          name: 'accesskey',
          description: 'Specifies a keyboard navigation accelerator for the element. Pressing ALT or a similar key in association with the specified character selects the form control correlated with that key sequence. Page designers are forewarned to avoid key sequences already bound to browsers. This attribute is global since HTML5.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/area',
        },
      ],
    },
    {
      name: 'table',
      description: {
        kind: 'markdown',
        value: 'The table element represents data with more than one dimension, in the form of a table.',
      },
      attributes: [
        {
          name: 'border',
        },
        {
          name: 'align',
          description: 'This enumerated attribute indicates how the table must be aligned inside the containing document. It may have the following values:\n\n*   left: the table is displayed on the left side of the document;\n*   center: the table is displayed in the center of the document;\n*   right: the table is displayed on the right side of the document.\n\n**Usage Note**\n\n*   **Do not use this attribute**, as it has been deprecated. The [`<table>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table "The HTML <table> element represents tabular data — that is, information presented in a two-dimensional table comprised of rows and columns of cells containing data.") element should be styled using [CSS](https://developer.mozilla.org/en-US/docs/CSS). Set [`margin-left`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left "The margin-left CSS property sets the margin area on the left side of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") and [`margin-right`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right "The margin-right CSS property sets the margin area on the right side of an element. A positive value places it farther from its neighbors, while a negative value places it closer.") to `auto` or [`margin`](https://developer.mozilla.org/en-US/docs/Web/CSS/margin "The margin CSS property sets the margin area on all four sides of an element. It is a shorthand for margin-top, margin-right, margin-bottom, and margin-left.") to `0 auto` to achieve an effect that is similar to the align attribute.\n*   Prior to Firefox 4, Firefox also supported the `middle`, `absmiddle`, and `abscenter` values as synonyms of `center`, in quirks mode only.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/table',
        },
      ],
    },
    {
      name: 'caption',
      description: {
        kind: 'markdown',
        value: 'The caption element represents the title of the table that is its parent, if it has a parent and that is a table element.',
      },
      attributes: [
        {
          name: 'align',
          description: 'This enumerated attribute indicates how the caption must be aligned with respect to the table. It may have one of the following values:\n\n`left`\n\nThe caption is displayed to the left of the table.\n\n`top`\n\nThe caption is displayed above the table.\n\n`right`\n\nThe caption is displayed to the right of the table.\n\n`bottom`\n\nThe caption is displayed below the table.\n\n**Usage note:** Do not use this attribute, as it has been deprecated. The [`<caption>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/caption "The HTML Table Caption element (<caption>) specifies the caption (or title) of a table, and if used is always the first child of a <table>.") element should be styled using the [CSS](https://developer.mozilla.org/en-US/docs/CSS) properties [`caption-side`](https://developer.mozilla.org/en-US/docs/Web/CSS/caption-side "The caption-side CSS property puts the content of a table\'s <caption> on the specified side. The values are relative to the writing-mode of the table.") and [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.").',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/caption',
        },
      ],
    },
    {
      name: 'colgroup',
      description: {
        kind: 'markdown',
        value: 'The colgroup element represents a group of one or more columns in the table that is its parent, if it has a parent and that is a table element.',
      },
      attributes: [
        {
          name: 'span',
        },
        {
          name: 'align',
          description: 'This enumerated attribute specifies how horizontal alignment of each column cell content will be handled. Possible values are:\n\n*   `left`, aligning the content to the left of the cell\n*   `center`, centering the content in the cell\n*   `right`, aligning the content to the right of the cell\n*   `justify`, inserting spaces into the textual content so that the content is justified in the cell\n*   `char`, aligning the textual content on a special character with a minimal offset, defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-charoff) attributes Unimplemented (see [bug 2212](https://bugzilla.mozilla.org/show_bug.cgi?id=2212 "character alignment not implemented (align=char, charoff=, text-align:<string>)")).\n\nIf this attribute is not set, the `left` value is assumed. The descendant [`<col>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col "The HTML <col> element defines a column within a table and is used for defining common semantics on all common cells. It is generally found within a <colgroup> element.") elements may override this value using their own [`align`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-align) attribute.\n\n**Note:** Do not use this attribute as it is obsolete (not supported) in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values:\n    *   Do not try to set the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property on a selector giving a [`<colgroup>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup "The HTML <colgroup> element defines a group of columns within a table.") element. Because [`<td>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td "The HTML <td> element defines a cell of a table that contains data. It participates in the table model.") elements are not descendant of the [`<colgroup>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup "The HTML <colgroup> element defines a group of columns within a table.") element, they won\'t inherit it.\n    *   If the table doesn\'t use a [`colspan`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-colspan) attribute, use one `td:nth-child(an+b)` CSS selector per column, where a is the total number of the columns in the table and b is the ordinal position of this column in the table. Only after this selector the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property can be used.\n    *   If the table does use a [`colspan`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-colspan) attribute, the effect can be achieved by combining adequate CSS attribute selectors like `[colspan=n]`, though this is not trivial.\n*   To achieve the same effect as the `char` value, in CSS3, you can use the value of the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup#attr-char) as the value of the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property Unimplemented.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/colgroup',
        },
      ],
    },
    {
      name: 'col',
      description: {
        kind: 'markdown',
        value: 'If a col element has a parent and that is a colgroup element that itself has a parent that is a table element, then the col element represents one or more columns in the column group represented by that colgroup.',
      },
      void: true,
      attributes: [
        {
          name: 'span',
        },
        {
          name: 'align',
          description: 'This enumerated attribute specifies how horizontal alignment of each column cell content will be handled. Possible values are:\n\n*   `left`, aligning the content to the left of the cell\n*   `center`, centering the content in the cell\n*   `right`, aligning the content to the right of the cell\n*   `justify`, inserting spaces into the textual content so that the content is justified in the cell\n*   `char`, aligning the textual content on a special character with a minimal offset, defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-charoff) attributes Unimplemented (see [bug 2212](https://bugzilla.mozilla.org/show_bug.cgi?id=2212 "character alignment not implemented (align=char, charoff=, text-align:<string>)")).\n\nIf this attribute is not set, its value is inherited from the [`align`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup#attr-align) of the [`<colgroup>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup "The HTML <colgroup> element defines a group of columns within a table.") element this `<col>` element belongs too. If there are none, the `left` value is assumed.\n\n**Note:** Do not use this attribute as it is obsolete (not supported) in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values:\n    *   Do not try to set the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property on a selector giving a [`<col>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col "The HTML <col> element defines a column within a table and is used for defining common semantics on all common cells. It is generally found within a <colgroup> element.") element. Because [`<td>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td "The HTML <td> element defines a cell of a table that contains data. It participates in the table model.") elements are not descendant of the [`<col>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col "The HTML <col> element defines a column within a table and is used for defining common semantics on all common cells. It is generally found within a <colgroup> element.") element, they won\'t inherit it.\n    *   If the table doesn\'t use a [`colspan`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-colspan) attribute, use the `td:nth-child(an+b)` CSS selector. Set `a` to zero and `b` to the position of the column in the table, e.g. `td:nth-child(2) { text-align: right; }` to right-align the second column.\n    *   If the table does use a [`colspan`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-colspan) attribute, the effect can be achieved by combining adequate CSS attribute selectors like `[colspan=n]`, though this is not trivial.\n*   To achieve the same effect as the `char` value, in CSS3, you can use the value of the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col#attr-char) as the value of the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property Unimplemented.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/col',
        },
      ],
    },
    {
      name: 'tbody',
      description: {
        kind: 'markdown',
        value: 'The tbody element represents a block of rows that consist of a body of data for the parent table element, if the tbody element has a parent and it is a table.',
      },
      attributes: [
        {
          name: 'align',
          description: 'This enumerated attribute specifies how horizontal alignment of each cell content will be handled. Possible values are:\n\n*   `left`, aligning the content to the left of the cell\n*   `center`, centering the content in the cell\n*   `right`, aligning the content to the right of the cell\n*   `justify`, inserting spaces into the textual content so that the content is justified in the cell\n*   `char`, aligning the textual content on a special character with a minimal offset, defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody#attr-charoff) attributes.\n\nIf this attribute is not set, the `left` value is assumed.\n\n**Note:** Do not use this attribute as it is obsolete (not supported) in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values, use the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property on it.\n*   To achieve the same effect as the `char` value, in CSS3, you can use the value of the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody#attr-char) as the value of the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property Unimplemented.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/tbody',
        },
      ],
    },
    {
      name: 'thead',
      description: {
        kind: 'markdown',
        value: 'The thead element represents the block of rows that consist of the column labels (headers) for the parent table element, if the thead element has a parent and it is a table.',
      },
      attributes: [
        {
          name: 'align',
          description: 'This enumerated attribute specifies how horizontal alignment of each cell content will be handled. Possible values are:\n\n*   `left`, aligning the content to the left of the cell\n*   `center`, centering the content in the cell\n*   `right`, aligning the content to the right of the cell\n*   `justify`, inserting spaces into the textual content so that the content is justified in the cell\n*   `char`, aligning the textual content on a special character with a minimal offset, defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead#attr-charoff) attributes Unimplemented (see [bug 2212](https://bugzilla.mozilla.org/show_bug.cgi?id=2212 "character alignment not implemented (align=char, charoff=, text-align:<string>)")).\n\nIf this attribute is not set, the `left` value is assumed.\n\n**Note:** Do not use this attribute as it is obsolete (not supported) in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values, use the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property on it.\n*   To achieve the same effect as the `char` value, in CSS3, you can use the value of the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead#attr-char) as the value of the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property Unimplemented.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/thead',
        },
      ],
    },
    {
      name: 'tfoot',
      description: {
        kind: 'markdown',
        value: 'The tfoot element represents the block of rows that consist of the column summaries (footers) for the parent table element, if the tfoot element has a parent and it is a table.',
      },
      attributes: [
        {
          name: 'align',
          description: 'This enumerated attribute specifies how horizontal alignment of each cell content will be handled. Possible values are:\n\n*   `left`, aligning the content to the left of the cell\n*   `center`, centering the content in the cell\n*   `right`, aligning the content to the right of the cell\n*   `justify`, inserting spaces into the textual content so that the content is justified in the cell\n*   `char`, aligning the textual content on a special character with a minimal offset, defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody#attr-charoff) attributes Unimplemented (see [bug 2212](https://bugzilla.mozilla.org/show_bug.cgi?id=2212 "character alignment not implemented (align=char, charoff=, text-align:<string>)")).\n\nIf this attribute is not set, the `left` value is assumed.\n\n**Note:** Do not use this attribute as it is obsolete (not supported) in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values, use the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property on it.\n*   To achieve the same effect as the `char` value, in CSS3, you can use the value of the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tfoot#attr-char) as the value of the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property Unimplemented.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/tfoot',
        },
      ],
    },
    {
      name: 'tr',
      description: {
        kind: 'markdown',
        value: 'The tr element represents a row of cells in a table.',
      },
      attributes: [
        {
          name: 'align',
          description: 'A [`DOMString`](https://developer.mozilla.org/en-US/docs/Web/API/DOMString "DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String.") which specifies how the cell\'s context should be aligned horizontally within the cells in the row; this is shorthand for using `align` on every cell in the row individually. Possible values are:\n\n`left`\n\nAlign the content of each cell at its left edge.\n\n`center`\n\nCenter the contents of each cell between their left and right edges.\n\n`right`\n\nAlign the content of each cell at its right edge.\n\n`justify`\n\nWiden whitespaces within the text of each cell so that the text fills the full width of each cell (full justification).\n\n`char`\n\nAlign each cell in the row on a specific character (such that each row in the column that is configured this way will horizontally align its cells on that character). This uses the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr#attr-charoff) to establish the alignment character (typically "." or "," when aligning numerical data) and the number of characters that should follow the alignment character. This alignment type was never widely supported.\n\nIf no value is expressly set for `align`, the parent node\'s value is inherited.\n\nInstead of using the obsolete `align` attribute, you should instead use the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property to establish `left`, `center`, `right`, or `justify` alignment for the row\'s cells. To apply character-based alignment, set the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property to the alignment character (such as `"."` or `","`).',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/tr',
        },
      ],
    },
    {
      name: 'td',
      description: {
        kind: 'markdown',
        value: 'The td element represents a data cell in a table.',
      },
      attributes: [
        {
          name: 'colspan',
        },
        {
          name: 'rowspan',
        },
        {
          name: 'headers',
        },
        {
          name: 'abbr',
          description: 'This attribute contains a short abbreviated description of the cell\'s content. Some user-agents, such as speech readers, may present this description before the content itself.\n\n**Note:** Do not use this attribute as it is obsolete in the latest standard. Alternatively, you can put the abbreviated description inside the cell and place the long content in the **title** attribute.',
        },
        {
          name: 'align',
          description: 'This enumerated attribute specifies how the cell content\'s horizontal alignment will be handled. Possible values are:\n\n*   `left`: The content is aligned to the left of the cell.\n*   `center`: The content is centered in the cell.\n*   `right`: The content is aligned to the right of the cell.\n*   `justify` (with text only): The content is stretched out inside the cell so that it covers its entire width.\n*   `char` (with text only): The content is aligned to a character inside the `<th>` element with minimal offset. This character is defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-charoff) attributes Unimplemented (see [bug 2212](https://bugzilla.mozilla.org/show_bug.cgi?id=2212 "character alignment not implemented (align=char, charoff=, text-align:<string>)")).\n\nThe default value when this attribute is not specified is `left`.\n\n**Note:** Do not use this attribute as it is obsolete in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values, apply the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property to the element.\n*   To achieve the same effect as the `char` value, give the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property the same value you would use for the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td#attr-char). Unimplemented in CSS3.',
        },
        {
          name: 'axis',
          description: 'This attribute contains a list of space-separated strings. Each string is the `id` of a group of cells that this header applies to.\n\n**Note:** Do not use this attribute as it is obsolete in the latest standard.',
        },
        {
          name: 'bgcolor',
          description: 'This attribute defines the background color of each cell in a column. It consists of a 6-digit hexadecimal code as defined in [sRGB](https://www.w3.org/Graphics/Color/sRGB) and is prefixed by \'#\'. This attribute may be used with one of sixteen predefined color strings:\n\n \n\n`black` = "#000000"\n\n \n\n`green` = "#008000"\n\n \n\n`silver` = "#C0C0C0"\n\n \n\n`lime` = "#00FF00"\n\n \n\n`gray` = "#808080"\n\n \n\n`olive` = "#808000"\n\n \n\n`white` = "#FFFFFF"\n\n \n\n`yellow` = "#FFFF00"\n\n \n\n`maroon` = "#800000"\n\n \n\n`navy` = "#000080"\n\n \n\n`red` = "#FF0000"\n\n \n\n`blue` = "#0000FF"\n\n \n\n`purple` = "#800080"\n\n \n\n`teal` = "#008080"\n\n \n\n`fuchsia` = "#FF00FF"\n\n \n\n`aqua` = "#00FFFF"\n\n**Note:** Do not use this attribute, as it is non-standard and only implemented in some versions of Microsoft Internet Explorer: The [`<td>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td "The HTML <td> element defines a cell of a table that contains data. It participates in the table model.") element should be styled using [CSS](https://developer.mozilla.org/en-US/docs/CSS). To create a similar effect use the [`background-color`](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color "The background-color CSS property sets the background color of an element.") property in [CSS](https://developer.mozilla.org/en-US/docs/CSS) instead.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/td',
        },
      ],
    },
    {
      name: 'th',
      description: {
        kind: 'markdown',
        value: 'The th element represents a header cell in a table.',
      },
      attributes: [
        {
          name: 'colspan',
        },
        {
          name: 'rowspan',
        },
        {
          name: 'headers',
        },
        {
          name: 'scope',
          valueSet: 's',
        },
        {
          name: 'sorted',
        },
        {
          name: 'abbr',
          description: {
            kind: 'markdown',
            value: 'This attribute contains a short abbreviated description of the cell\'s content. Some user-agents, such as speech readers, may present this description before the content itself.',
          },
        },
        {
          name: 'align',
          description: 'This enumerated attribute specifies how the cell content\'s horizontal alignment will be handled. Possible values are:\n\n*   `left`: The content is aligned to the left of the cell.\n*   `center`: The content is centered in the cell.\n*   `right`: The content is aligned to the right of the cell.\n*   `justify` (with text only): The content is stretched out inside the cell so that it covers its entire width.\n*   `char` (with text only): The content is aligned to a character inside the `<th>` element with minimal offset. This character is defined by the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th#attr-char) and [`charoff`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th#attr-charoff) attributes.\n\nThe default value when this attribute is not specified is `left`.\n\n**Note:** Do not use this attribute as it is obsolete in the latest standard.\n\n*   To achieve the same effect as the `left`, `center`, `right` or `justify` values, apply the CSS [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property to the element.\n*   To achieve the same effect as the `char` value, give the [`text-align`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align "The text-align CSS property sets the horizontal alignment of an inline or table-cell box. This means it works like vertical-align but in the horizontal direction.") property the same value you would use for the [`char`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th#attr-char). Unimplemented in CSS3.',
        },
        {
          name: 'axis',
          description: 'This attribute contains a list of space-separated strings. Each string is the `id` of a group of cells that this header applies to.\n\n**Note:** Do not use this attribute as it is obsolete in the latest standard: use the [`scope`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th#attr-scope) attribute instead.',
        },
        {
          name: 'bgcolor',
          description: 'This attribute defines the background color of each cell in a column. It consists of a 6-digit hexadecimal code as defined in [sRGB](https://www.w3.org/Graphics/Color/sRGB) and is prefixed by \'#\'. This attribute may be used with one of sixteen predefined color strings:\n\n \n\n`black` = "#000000"\n\n \n\n`green` = "#008000"\n\n \n\n`silver` = "#C0C0C0"\n\n \n\n`lime` = "#00FF00"\n\n \n\n`gray` = "#808080"\n\n \n\n`olive` = "#808000"\n\n \n\n`white` = "#FFFFFF"\n\n \n\n`yellow` = "#FFFF00"\n\n \n\n`maroon` = "#800000"\n\n \n\n`navy` = "#000080"\n\n \n\n`red` = "#FF0000"\n\n \n\n`blue` = "#0000FF"\n\n \n\n`purple` = "#800080"\n\n \n\n`teal` = "#008080"\n\n \n\n`fuchsia` = "#FF00FF"\n\n \n\n`aqua` = "#00FFFF"\n\n**Note:** Do not use this attribute, as it is non-standard and only implemented in some versions of Microsoft Internet Explorer: The [`<th>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th "The HTML <th> element defines a cell as header of a group of table cells. The exact nature of this group is defined by the scope and headers attributes.") element should be styled using [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS). To create a similar effect use the [`background-color`](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color "The background-color CSS property sets the background color of an element.") property in [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS) instead.',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/th',
        },
      ],
    },
    {
      name: 'button',
      description: {
        kind: 'markdown',
        value: 'The button element represents a button labeled by its contents.',
      },
      attributes: [
        {
          name: 'autofocus',
          valueSet: 'v',
          description: {
            kind: 'markdown',
            value: 'This Boolean attribute lets you specify that the button should have input focus when the page loads, unless the user overrides it, for example by typing in a different control. Only one form-associated element in a document can have this attribute specified.',
          },
        },
        {
          name: 'disabled',
          valueSet: 'v',
          description: {
            kind: 'markdown',
            value: 'This Boolean attribute indicates that the user cannot interact with the button. If this attribute is not specified, the button inherits its setting from the containing element, for example [`<fieldset>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/fieldset "The HTML <fieldset> element is used to group several controls as well as labels (<label>) within a web form."); if there is no containing element with the **disabled** attribute set, then the button is enabled.\n\nFirefox will, unlike other browsers, by default, [persist the dynamic disabled state](https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing) of a [`<button>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button "The HTML <button> element represents a clickable button, which can be used in forms or anywhere in a document that needs simple, standard button functionality.") across page loads. Use the [`autocomplete`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-autocomplete) attribute to control this feature.',
          },
        },
        {
          name: 'name',
          description: {
            kind: 'markdown',
            value: 'The name of the button, which is submitted with the form data.',
          },
        },
        {
          name: 'type',
          valueSet: 'bt',
          description: {
            kind: 'markdown',
            value: 'The type of the button. Possible values are:\n\n*   `submit`: The button submits the form data to the server. This is the default if the attribute is not specified, or if the attribute is dynamically changed to an empty or invalid value.\n*   `reset`: The button resets all the controls to their initial values.\n*   `button`: The button has no default behavior. It can have client-side scripts associated with the element\'s events, which are triggered when the events occur.',
          },
        },
        {
          name: 'value',
          description: {
            kind: 'markdown',
            value: 'The initial value of the button. It defines the value associated with the button which is submitted with the form data. This value is passed to the server in params when the form is submitted.',
          },
        },
        {
          name: 'autocomplete',
          description: 'The use of this attribute on a [`<button>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button "The HTML <button> element represents a clickable button, which can be used in forms or anywhere in a document that needs simple, standard button functionality.") is nonstandard and Firefox-specific. By default, unlike other browsers, [Firefox persists the dynamic disabled state](https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing) of a [`<button>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button "The HTML <button> element represents a clickable button, which can be used in forms or anywhere in a document that needs simple, standard button functionality.") across page loads. Setting the value of this attribute to `off` (i.e. `autocomplete="off"`) disables this feature. See [bug 654072](https://bugzilla.mozilla.org/show_bug.cgi?id=654072 "if disabled state is changed with javascript, the normal state doesn\'t return after refreshing the page").',
        },
      ],
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Element/button',
        },
      ],
    },
  ],
  globalAttributes: [
    {
      name: 'class',
      description: {
        kind: 'markdown',
        value: 'A space-separated list of the classes of the element. Classes allows CSS and JavaScript to select and access specific elements via the [class selectors](https://developer.mozilla.org/docs/Web/CSS/Class_selectors) or functions like the method [`Document.getElementsByClassName()`](https://developer.mozilla.org/docs/Web/API/Document/getElementsByClassName "returns an array-like object of all child elements which have all of the given class names.").',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/class',
        },
      ],
    },
    {
      name: 'dir',
      description: {
        kind: 'markdown',
        value: 'An enumerated attribute indicating the directionality of the element\'s text. It can have the following values:\n\n*   `ltr`, which means _left to right_ and is to be used for languages that are written from the left to the right (like English);\n*   `rtl`, which means _right to left_ and is to be used for languages that are written from the right to the left (like Arabic);\n*   `auto`, which lets the user agent decide. It uses a basic algorithm as it parses the characters inside the element until it finds a character with a strong directionality, then it applies that directionality to the whole element.',
      },
      valueSet: 'd',
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/dir',
        },
      ],
    },
    {
      name: 'hidden',
      description: {
        kind: 'markdown',
        value: 'A Boolean attribute indicates that the element is not yet, or is no longer, _relevant_. For example, it can be used to hide elements of the page that can\'t be used until the login process has been completed. The browser won\'t render such elements. This attribute must not be used to hide content that could legitimately be shown.',
      },
      valueSet: 'v',
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/hidden',
        },
      ],
    },
    {
      name: 'id',
      description: {
        kind: 'markdown',
        value: 'Defines a unique identifier (ID) which must be unique in the whole document. Its purpose is to identify the element when linking (using a fragment identifier), scripting, or styling (with CSS).',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/id',
        },
      ],
    },
    {
      name: 'is',
      description: {
        kind: 'markdown',
        value: 'Allows you to specify that a standard HTML element should behave like a registered custom built-in element (see [Using custom elements](https://developer.mozilla.org/docs/Web/Web_Components/Using_custom_elements) for more details).',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/is',
        },
      ],
    },
    {
      name: 'lang',
      description: {
        kind: 'markdown',
        value: 'Helps define the language of an element: the language that non-editable elements are in, or the language that editable elements should be written in by the user. The attribute contains one “language tag” (made of hyphen-separated “language subtags”) in the format defined in [_Tags for Identifying Languages (BCP47)_](https://www.ietf.org/rfc/bcp/bcp47.txt). [**xml:lang**](#attr-xml:lang) has priority over it.',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/lang',
        },
      ],
    },
    {
      name: 'role',
      valueSet: 'roles',
    },
    {
      name: 'style',
      description: {
        kind: 'markdown',
        value: 'Contains [CSS](https://developer.mozilla.org/docs/Web/CSS) styling declarations to be applied to the element. Note that it is recommended for styles to be defined in a separate file or files. This attribute and the [`<style>`](https://developer.mozilla.org/docs/Web/HTML/Element/style "The HTML <style> element contains style information for a document, or part of a document.") element have mainly the purpose of allowing for quick styling, for example for testing purposes.',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/style',
        },
      ],
    },
    {
      name: 'tabindex',
      description: {
        kind: 'markdown',
        value: 'An integer attribute indicating if the element can take input focus (is _focusable_), if it should participate to sequential keyboard navigation, and if so, at what position. It can take several values:\n\n*   a _negative value_ means that the element should be focusable, but should not be reachable via sequential keyboard navigation;\n*   `0` means that the element should be focusable and reachable via sequential keyboard navigation, but its relative order is defined by the platform convention;\n*   a _positive value_ means that the element should be focusable and reachable via sequential keyboard navigation; the order in which the elements are focused is the increasing value of the [**tabindex**](#attr-tabindex). If several elements share the same tabindex, their relative order follows their relative positions in the document.',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/tabindex',
        },
      ],
    },
    {
      name: 'title',
      description: {
        kind: 'markdown',
        value: 'Contains a text representing advisory information related to the element it belongs to. Such information can typically, but not necessarily, be presented to the user as a tooltip.',
      },
      references: [
        {
          name: 'MDN Reference',
          url: 'https://developer.mozilla.org/docs/Web/HTML/Global_attributes/title',
        },
      ],
    },
    {
      name: 'aria-colcount',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-colcount',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines the total number of columns in a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-colindex`](https://www.w3.org/TR/wai-aria-1.1/#aria-colindex).',
      },
    },
    {
      name: 'aria-colindex',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-colindex',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines an [element\'s](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) column index or position with respect to the total number of columns within a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-colcount`](https://www.w3.org/TR/wai-aria-1.1/#aria-colcount) and [`aria-colspan`](https://www.w3.org/TR/wai-aria-1.1/#aria-colspan).',
      },
    },
    {
      name: 'aria-colspan',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-colspan',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines the number of columns spanned by a cell or gridcell within a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-colindex`](https://www.w3.org/TR/wai-aria-1.1/#aria-colindex) and [`aria-rowspan`](https://www.w3.org/TR/wai-aria-1.1/#aria-rowspan).',
      },
    },
    {
      name: 'aria-disabled',
      valueSet: 'b',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-disabled',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Indicates that the [element](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) is [perceivable](https://www.w3.org/TR/wai-aria-1.1/#dfn-perceivable) but disabled, so it is not editable or otherwise [operable](https://www.w3.org/TR/wai-aria-1.1/#dfn-operable). See related [`aria-hidden`](https://www.w3.org/TR/wai-aria-1.1/#aria-hidden) and [`aria-readonly`](https://www.w3.org/TR/wai-aria-1.1/#aria-readonly).',
      },
    },
    {
      name: 'aria-hidden',
      valueSet: 'b',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-hidden',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Indicates whether the [element](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) is exposed to an accessibility API. See related [`aria-disabled`](https://www.w3.org/TR/wai-aria-1.1/#aria-disabled).',
      },
    },
    {
      name: 'aria-label',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-label',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines a string value that labels the current element. See related [`aria-labelledby`](https://www.w3.org/TR/wai-aria-1.1/#aria-labelledby).',
      },
    },
    {
      name: 'aria-labelledby',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-labelledby',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Identifies the [element](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) (or elements) that labels the current element. See related [`aria-describedby`](https://www.w3.org/TR/wai-aria-1.1/#aria-describedby).',
      },
    },
    {
      name: 'aria-live',
      valueSet: 'live',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-live',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Indicates that an [element](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) will be updated, and describes the types of updates the [user agents](https://www.w3.org/TR/wai-aria-1.1/#dfn-user-agent), [assistive technologies](https://www.w3.org/TR/wai-aria-1.1/#dfn-assistive-technology), and user can expect from the [live region](https://www.w3.org/TR/wai-aria-1.1/#dfn-live-region).',
      },
    },
    {
      name: 'aria-orientation',
      valueSet: 'orientation',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-orientation',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Indicates whether the element\'s orientation is horizontal, vertical, or unknown/ambiguous.',
      },
    },
    {
      name: 'aria-readonly',
      valueSet: 'b',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-readonly',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Indicates that the [element](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) is not editable, but is otherwise [operable](https://www.w3.org/TR/wai-aria-1.1/#dfn-operable). See related [`aria-disabled`](https://www.w3.org/TR/wai-aria-1.1/#aria-disabled).',
      },
    },
    {
      name: 'aria-rowcount',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-rowcount',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines the total number of rows in a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-rowindex`](https://www.w3.org/TR/wai-aria-1.1/#aria-rowindex).',
      },
    },
    {
      name: 'aria-rowindex',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-rowindex',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines an [element\'s](https://www.w3.org/TR/wai-aria-1.1/#dfn-element) row index or position with respect to the total number of rows within a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-rowcount`](https://www.w3.org/TR/wai-aria-1.1/#aria-rowcount) and [`aria-rowspan`](https://www.w3.org/TR/wai-aria-1.1/#aria-rowspan).',
      },
    },
    {
      name: 'aria-rowspan',
      references: [
        {
          name: 'WAI-ARIA Reference',
          url: 'https://www.w3.org/TR/wai-aria-1.1/#aria-rowspan',
        },
      ],
      description: {
        kind: 'markdown',
        value: 'Defines the number of rows spanned by a cell or gridcell within a [`table`](https://www.w3.org/TR/wai-aria-1.1/#table), [`grid`](https://www.w3.org/TR/wai-aria-1.1/#grid), or [`treegrid`](https://www.w3.org/TR/wai-aria-1.1/#treegrid). See related [`aria-rowindex`](https://www.w3.org/TR/wai-aria-1.1/#aria-rowindex) and [`aria-colspan`](https://www.w3.org/TR/wai-aria-1.1/#aria-colspan).',
      },
    },
  ],
  valueSets: [
    {
      name: 'b',
      values: [
        {
          name: 'true',
        },
        {
          name: 'false',
        },
      ],
    },
    {
      name: 'd',
      values: [
        {
          name: 'ltr',
        },
        {
          name: 'rtl',
        },
        {
          name: 'auto',
        },
      ],
    },
    {
      name: 's',
      values: [
        {
          name: 'row',
        },
        {
          name: 'col',
        },
        {
          name: 'rowgroup',
        },
        {
          name: 'colgroup',
        },
      ],
    },
    {
      name: 't',
      values: [
        {
          name: 'hidden',
        },
        {
          name: 'text',
        },
        {
          name: 'search',
        },
        {
          name: 'tel',
        },
        {
          name: 'url',
        },
        {
          name: 'email',
        },
        {
          name: 'password',
        },
        {
          name: 'datetime',
        },
        {
          name: 'date',
        },
        {
          name: 'month',
        },
        {
          name: 'week',
        },
        {
          name: 'time',
        },
        {
          name: 'datetime-local',
        },
        {
          name: 'number',
        },
        {
          name: 'range',
        },
        {
          name: 'color',
        },
        {
          name: 'checkbox',
        },
        {
          name: 'radio',
        },
        {
          name: 'file',
        },
        {
          name: 'submit',
        },
        {
          name: 'image',
        },
        {
          name: 'reset',
        },
        {
          name: 'button',
        },
      ],
    },
    {
      name: 'bt',
      values: [
        {
          name: 'button',
        },
        {
          name: 'submit',
        },
        {
          name: 'reset',
        },
        {
          name: 'menu',
        },
      ],
    },
    {
      name: 'lt',
      values: [
        {
          name: '1',
        },
        {
          name: 'a',
        },
        {
          name: 'A',
        },
        {
          name: 'i',
        },
        {
          name: 'I',
        },
      ],
    },
    {
      name: 'mt',
      values: [
        {
          name: 'context',
        },
        {
          name: 'toolbar',
        },
      ],
    },
    {
      name: 'mit',
      values: [
        {
          name: 'command',
        },
        {
          name: 'checkbox',
        },
        {
          name: 'radio',
        },
      ],
    },
    {
      name: 'sh',
      values: [
        {
          name: 'circle',
        },
        {
          name: 'default',
        },
        {
          name: 'poly',
        },
        {
          name: 'rect',
        },
      ],
    },
    {
      name: 'xo',
      values: [
        {
          name: 'anonymous',
        },
        {
          name: 'use-credentials',
        },
      ],
    },
    {
      name: 'target',
      values: [
        {
          name: '_self',
        },
        {
          name: '_blank',
        },
        {
          name: '_parent',
        },
        {
          name: '_top',
        },
      ],
    },
    {
      name: 'live',
      values: [
        {
          name: 'off',
        },
        {
          name: 'polite',
        },
        {
          name: 'assertive',
        },
      ],
    },
    {
      name: 'orientation',
      values: [
        {
          name: 'vertical',
        },
        {
          name: 'horizontal',
        },
        {
          name: 'undefined',
        },
      ],
    },
    {
      name: 'roles',
      values: [
        {
          name: 'alert',
        },
        {
          name: 'alertdialog',
        },
        {
          name: 'button',
        },
        {
          name: 'checkbox',
        },
        {
          name: 'dialog',
        },
        {
          name: 'gridcell',
        },
        {
          name: 'link',
        },
        {
          name: 'log',
        },
        {
          name: 'marquee',
        },
        {
          name: 'menuitem',
        },
        {
          name: 'menuitemcheckbox',
        },
        {
          name: 'menuitemradio',
        },
        {
          name: 'option',
        },
        {
          name: 'progressbar',
        },
        {
          name: 'radio',
        },
        {
          name: 'scrollbar',
        },
        {
          name: 'searchbox',
        },
        {
          name: 'slider',
        },
        {
          name: 'spinbutton',
        },
        {
          name: 'status',
        },
        {
          name: 'switch',
        },
        {
          name: 'tab',
        },
        {
          name: 'tabpanel',
        },
        {
          name: 'textbox',
        },
        {
          name: 'timer',
        },
        {
          name: 'tooltip',
        },
        {
          name: 'treeitem',
        },
        {
          name: 'combobox',
        },
        {
          name: 'grid',
        },
        {
          name: 'listbox',
        },
        {
          name: 'menu',
        },
        {
          name: 'menubar',
        },
        {
          name: 'radiogroup',
        },
        {
          name: 'tablist',
        },
        {
          name: 'tree',
        },
        {
          name: 'treegrid',
        },
        {
          name: 'application',
        },
        {
          name: 'article',
        },
        {
          name: 'cell',
        },
        {
          name: 'columnheader',
        },
        {
          name: 'definition',
        },
        {
          name: 'directory',
        },
        {
          name: 'document',
        },
        {
          name: 'feed',
        },
        {
          name: 'figure',
        },
        {
          name: 'group',
        },
        {
          name: 'heading',
        },
        {
          name: 'img',
        },
        {
          name: 'list',
        },
        {
          name: 'listitem',
        },
        {
          name: 'math',
        },
        {
          name: 'none',
        },
        {
          name: 'note',
        },
        {
          name: 'presentation',
        },
        {
          name: 'region',
        },
        {
          name: 'row',
        },
        {
          name: 'rowgroup',
        },
        {
          name: 'rowheader',
        },
        {
          name: 'separator',
        },
        {
          name: 'table',
        },
        {
          name: 'term',
        },
        {
          name: 'text',
        },
        {
          name: 'toolbar',
        },
        {
          name: 'banner',
        },
        {
          name: 'complementary',
        },
        {
          name: 'contentinfo',
        },
        {
          name: 'form',
        },
        {
          name: 'main',
        },
        {
          name: 'navigation',
        },
        {
          name: 'region',
        },
        {
          name: 'search',
        },
        {
          name: 'doc-abstract',
        },
        {
          name: 'doc-acknowledgments',
        },
        {
          name: 'doc-afterword',
        },
        {
          name: 'doc-appendix',
        },
        {
          name: 'doc-backlink',
        },
        {
          name: 'doc-biblioentry',
        },
        {
          name: 'doc-bibliography',
        },
        {
          name: 'doc-biblioref',
        },
        {
          name: 'doc-chapter',
        },
        {
          name: 'doc-colophon',
        },
        {
          name: 'doc-conclusion',
        },
        {
          name: 'doc-cover',
        },
        {
          name: 'doc-credit',
        },
        {
          name: 'doc-credits',
        },
        {
          name: 'doc-dedication',
        },
        {
          name: 'doc-endnote',
        },
        {
          name: 'doc-endnotes',
        },
        {
          name: 'doc-epigraph',
        },
        {
          name: 'doc-epilogue',
        },
        {
          name: 'doc-errata',
        },
        {
          name: 'doc-example',
        },
        {
          name: 'doc-footnote',
        },
        {
          name: 'doc-foreword',
        },
        {
          name: 'doc-glossary',
        },
        {
          name: 'doc-glossref',
        },
        {
          name: 'doc-index',
        },
        {
          name: 'doc-introduction',
        },
        {
          name: 'doc-noteref',
        },
        {
          name: 'doc-notice',
        },
        {
          name: 'doc-pagebreak',
        },
        {
          name: 'doc-pagelist',
        },
        {
          name: 'doc-part',
        },
        {
          name: 'doc-preface',
        },
        {
          name: 'doc-prologue',
        },
        {
          name: 'doc-pullquote',
        },
        {
          name: 'doc-qna',
        },
        {
          name: 'doc-subtitle',
        },
        {
          name: 'doc-tip',
        },
        {
          name: 'doc-toc',
        },
      ],
    },
    {
      name: 'metanames',
      values: [
        {
          name: 'application-name',
        },
        {
          name: 'author',
        },
        {
          name: 'description',
        },
        {
          name: 'format-detection',
        },
        {
          name: 'generator',
        },
        {
          name: 'keywords',
        },
        {
          name: 'publisher',
        },
        {
          name: 'referrer',
        },
        {
          name: 'robots',
        },
        {
          name: 'theme-color',
        },
        {
          name: 'viewport',
        },
      ],
    },
    {
      name: 'decoding',
      values: [
        {
          name: 'sync',
        },
        {
          name: 'async',
        },
        {
          name: 'auto',
        },
      ],
    },
    {
      name: 'loading',
      values: [
        {
          name: 'eager',
        },
        {
          name: 'lazy',
        },
      ],
    },
    {
      name: 'referrerpolicy',
      values: [
        {
          name: 'no-referrer',
        },
        {
          name: 'no-referrer-when-downgrade',
        },
        {
          name: 'origin',
        },
        {
          name: 'origin-when-cross-origin',
        },
        {
          name: 'same-origin',
        },
        {
          name: 'strict-origin',
        },
        {
          name: 'strict-origin-when-cross-origin',
        },
        {
          name: 'unsafe-url',
        },
      ],
    },
  ],
}
