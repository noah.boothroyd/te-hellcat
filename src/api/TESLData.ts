import type { INamespace, IProperty } from 'types/tags'

import allData from 'data/tags/allData'

class TESLData {
  public readonly unflattenedNamespaceData: INamespace[] = []
  public readonly flattenedNamespaceData: INamespace[] = []
  public readonly flattenedPropertyData: IProperty[] = []

  private constructor(data: INamespace[]) {
    this.unflattenedNamespaceData = data
    this.flattenedNamespaceData = this.flattenNamespaces(this.unflattenedNamespaceData)
    this.flattenedPropertyData = this.flattenProperties(this.flattenedNamespaceData)
  }

  static async initialize() {
    // Replace allData with actual GET request later
    // Log success or failure to datadog
    return new TESLData(allData)
  }

  private flattenNamespaces(list: INamespace[]): INamespace[] {
    const flattenedNamespaces = list.flatMap(item => item.subnamespaces?.length ? [item, ...this.flattenNamespaces(item.subnamespaces)] : item)
    return flattenedNamespaces
  }

  private flattenProperties(flatList: INamespace[]): IProperty[] {
    const flattenedProperties = flatList.flatMap(item => item.properties)
    return flattenedProperties
  }

  getAllNamespaceNames(): string[] {
    return this.flattenedNamespaceData.map(item => item.name)
  }

  getAllPropertyNames(dedupe = false): string[] {
    const propertyNames = this.flattenedPropertyData.map(item => item.property)
    return dedupe ? propertyNames.filter((item, index) => propertyNames.indexOf(item) === index) : propertyNames
  }

  getNamespaceObjByName(name: string): INamespace | undefined {
    return this.flattenedNamespaceData.find(item => item.name = name)
  }

  getPropertyObjByProperty(property: string): IProperty | undefined {
    return this.flattenedPropertyData.find(item => item.property = property)
  }

  getPropertiesByNamespace(namespace: string): IProperty[] | undefined {
    return this.flattenedNamespaceData.find(item => item.name === namespace)?.properties
  }
}

export const TESLDataInstance = await TESLData.initialize()
