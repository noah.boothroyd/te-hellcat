export default [
  {
    label: 'Branded - Company Name',
    property: 'brand_company_name',
    type: 'text',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
  {
    label: 'Branded - EH Logo',
    property: 'brand_eh_logo',
    type: 'url',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
  {
    label: 'Branded - Primary Color',
    property: 'brand_primary_color_hex',
    type: 'text',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
  {
    label: 'Branded - Secondary Color',
    property: 'brand_secondary_color_hex',
    type: 'text',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
  {
    label: 'Branded - Email Disclaimer',
    property: 'brand_email_disclaimer',
    type: 'text',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
  {
    label: 'Branded - Company Logo',
    property: 'brand_user_logo',
    type: 'url',
    location: 'Branding Snippet',
    hint: 'Hint',
  },
]
