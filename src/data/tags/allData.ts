// import brandTags from './brandTags'
import orgTags from './orgTags'
import contactTags from './contactTags'
import loanTags from './loanTags'
import userTags from './userTags'
import insightTags from './insightTags'

export default [
  {
    name: 'sender',
    label: 'Sender',
    hint: 'The `sender` object represents the user on the TE Platform doing the email sending.',
    subnamespaces: [
      {
        name: 'disclaimers',
        label: 'Disclaimers',
        parent: 'sender',
        hint: 'The `disclaimers` object represents user disclaimers associated with the sender.',
        properties: [],
      },
    ],
    properties: userTags,
  },
  {
    name: 'recipient',
    label: 'Recipient',
    hint: 'The `recipient` object represents the lead the designed email is being sent to.',
    subnamespaces: [
      {
        name: 'insight',
        label: 'Insight',
        parent: 'recipient',
        hint: 'The `insight` object represents data obtained from one of the Insight Alerts associated with this template\'s journey and its recipient.',
        properties: insightTags,
      },
    ],
    properties: contactTags,
  },
  {
    name: 'comarketer',
    label: 'Co-Marketer',
    hint: 'The `comarketer` object represents a marketing partner connected to the Total Expert user via a Co-Marketing Invite.',
    properties: userTags,
  },
  {
    name: 'organization',
    label: 'Organization',
    hint: 'The `organization` object represents the data of the Total Expert user\'s organization.',
    properties: orgTags,
  },
  // {
  //   name: 'brand',
  //   label: 'Brand',
  //   hint: 'Hint',
  //   properties: brandTags,
  // },
  {
    name: 'loan',
    label: 'Loan',
    hint: 'The `loan` object represents data from the loan product attached to this template\'s journey.',
    subnamespaces: [
      {
        name: 'attorney',
        label: 'Attorney',
        parent: 'loan',
        hint: 'The `attorney` object represents data from the Attorney contact associated with the loan product.',
        properties: contactTags,
      },
      {
        name: 'borrower',
        label: 'Borrower',
        parent: 'loan',
        hint: 'The `borrower` object represents data from the Borrower contact associated with the loan product.',
        properties: contactTags,
      },
      {
        name: 'buyers_agent',
        label: 'Buyer\'s Agent',
        parent: 'loan',
        hint: 'The `buyers_agent` object represents data from the Buyer\'s Agent contact associated with the loan product.',
        properties: contactTags,
      },
      {
        name: 'coborrower',
        label: 'Co-Borrower',
        parent: 'loan',
        hint: 'The `coborrower` object represents data from the Co-Borrower contact associated with the loan product.',
        properties: contactTags,
      },
      {
        name: 'sellers_agent',
        label: 'Seller\'s Agent',
        parent: 'loan',
        hint: 'The `sellers_agent` object represents data from the Seller\'s Agent contact associated with the loan product.',
        properties: contactTags,
      },
      {
        name: 'settlement_agent',
        label: 'Settlement Agent',
        parent: 'loan',
        hint: 'The `settlement_agent` object represents data from the Settlement Agent contact associated with the loan product.',
        properties: contactTags,
      },
    ],
    properties: loanTags,
  },
]
