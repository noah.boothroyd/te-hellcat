export const truncateEllipse = (text: string, maximum = 250) => {
  return text.substring(0, maximum - 1) + (text.length > maximum ? '&hellip;' : '')
}
