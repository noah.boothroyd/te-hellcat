import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import TSconfigPaths from 'vite-tsconfig-paths'
import Unocss from 'unocss/vite'
import { HeadlessUiResolver, NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import Monaco from 'vite-plugin-monaco-editor'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    Vue(),
    Unocss(),
    TSconfigPaths(),
    AutoImport({
      imports: [
        'vue',
        '@vueuse/core',
        'pinia',
      ],
      dirs: [
        'src/helpers',
      ],
      dts: 'src/auto-imports.d.ts',
    }),
    Components({
      dirs: ['src/components', 'src/modules', 'src/layout'],
      deep: true,
      include: [/\.vue$/, /\.vue\?vue/],
      resolvers: [HeadlessUiResolver(), NaiveUiResolver()],
      dts: 'src/components.d.ts',
    }),
    Monaco.default({
      languageWorkers: ['editorWorkerService', 'html'],
    }),
  ],
})
